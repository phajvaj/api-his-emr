exports.options = {
    routePrefix: '/swagger',
    swagger: {
        info: {
            title: 'hie swagger',
            description: 'hie the swagger API',
            version: '0.1.0'
        },
        externalDocs: {
            url: 'https://swagger.io',
            description: 'Find more info here'
        },
        host: 'localhost',
        schemes: ['http'],
        consumes: ['application/json'],
        produces: ['application/json'],
    },
    exposeRoute: true
};