# ระบบ NAN API Client

## Installation

```
git clone https://gitlab.com/phajvaj/api-his-emr.git
cp config.txt config
npm install

#PM2
$ pm2 start nodemon -n "app name"
```

Edit `config` file for connection/his configuration.

## Running

```
nodemon
```
