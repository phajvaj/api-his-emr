FROM node:14-alpine

ENV LD_LIBRARY_PATH=/lib

LABEL maintainer="Banjong Kittisawangwong<saciw.doi@gmail.com>"
WORKDIR /home/api
RUN apk update && \
    apk upgrade && \
    apk add --no-cache \
    alpine-sdk git curl py-pip \
    build-base libtool autoconf \
    automake gzip g++ \
    make tzdata \
    && cp /usr/share/zoneinfo/Asia/Bangkok /etc/localtime \
    && echo "Asia/Bangkok" > /etc/timezone

RUN wget https://download.oracle.com/otn_software/linux/instantclient/193000/instantclient-basic-linux.x64-19.3.0.0.0dbru.zip && \
    unzip instantclient-basic-linux.x64-19.3.0.0.0dbru.zip && \
    cp -r instantclient_19_3/* /lib && \
    rm -rf instantclient-basic-linux.x64-19.3.0.0.0dbru.zip && \
    apk add libaio && \
    apk add libaio libnsl libc6-compat && \
    cd /lib && \
    # Linking ld-linux-x86-64.so.2 to the lib/ location (Update accordingly)
    ln -s /lib64/* /lib && \
    ln -s libnsl.so.2 /usr/lib/libnsl.so.1 && \
    ln -s libc.so /usr/lib/libresolv.so.2

COPY . .

RUN mkdir -p /home/api/public && chmod 0775 /home/api/public
RUN npm i && npm run build:dist
RUN node --version

EXPOSE 5500

CMD ["node", "./dist/app.js"]
