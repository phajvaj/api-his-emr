import { FastifyInstance } from 'fastify';

export default async function router(fastify: FastifyInstance) {
    fastify.register(require('./routes/index'), { prefix: '/', logger: true });
    fastify.register(require('./routes/login'), { prefix: '/login', logger: true });
    fastify.register(require('./routes/token'), { prefix: '/token', logger: true });
    fastify.register(require('./routes/labs'), { prefix: '/labs', logger: true });
    fastify.register(require('./routes/drugs'), { prefix: '/drugs', logger: true });
    fastify.register(require('./routes/visits'), { prefix: '/visits', logger: true });
    fastify.register(require('./routes/biopsys'), { prefix: '/biopsys', logger: true });
    fastify.register(require('./routes/xrays'), { prefix: '/xrays', logger: true });
}
