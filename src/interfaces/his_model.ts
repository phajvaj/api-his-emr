import {Hosxpv3Model} from '../models/hosxpv3';
import {NanhisModel} from '../models/nanhis';
import {Hosxpv4Model} from '../models/hosxpv4';
import {Hosxpv4pgModel} from '../models/hosxpv4pg';
import {HososModel} from '../models/hosos';
import {HomcModel} from '../models/homc';
import {JhosModel} from '../models/jhos';
import {PmkModel} from '../models/pmk';
import {HosxppcuModel} from '../models/hosxppcu';
import {JhcisModel} from '../models/jhcis';
import {SsbModel} from '../models/ssb';
import {Ssb2Model} from '../models/ssb2';
import {HisModel} from '../models/his.model';
import {SoftconModel} from '../models/softcon';

export class His_model {
  getModel () {
    const his = process.env.HIS_PROVIDER || 'hosxpv3';

    let hisModel: any = new HisModel();
    switch (his) {
      case 'nanhis':
        hisModel = new NanhisModel();
        break;
      case 'hosxpv3':
        hisModel = new Hosxpv3Model();
        break;
      case 'hosxpv4':
        hisModel = new Hosxpv4Model();
        break;
      case 'hosxpv4pg':
        hisModel = new Hosxpv4pgModel();
        break;
      case 'ssb':
        hisModel = new SsbModel();
        break;
      case 'ssb2':
        hisModel = new Ssb2Model();
        break;
      case 'jhcis':
        hisModel = new JhcisModel();
        break;
      case 'hosxppcu':
        hisModel = new HosxppcuModel();
        break;
      case 'hospitalos':
        hisModel = new HososModel();
        break;
      case 'jhos':
        hisModel = new JhosModel();
        break;
      case 'pmk':
        hisModel = new PmkModel();
        break;
      case 'homc':
        hisModel = new HomcModel();
        break;
      case 'softcon':
        hisModel = new SoftconModel();
        break;
      default:
        hisModel = new Hosxpv3Model();
    }

    return hisModel;
  }
}
