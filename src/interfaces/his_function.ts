export class His_function {
  getResultNot0 () {
    const his = process.env.HIS_PROVIDER || 'hosxpv3';

    const idx = ['homc', 'ssb', 'ssb2', 'softcon', 'pmk'];

    return idx.includes(his);
  }
}
