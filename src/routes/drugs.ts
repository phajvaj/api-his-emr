/// <reference path="../../typings.d.ts" />

import * as Knex from 'knex';
import * as fastify from 'fastify';
import * as HttpStatus from 'http-status-codes';
import {His_model} from '../interfaces/his_model';
import {His_function} from '../interfaces/his_function';

const resultNot0 = new His_function().getResultNot0();
const _model = new His_model().getModel();

const router = (fastify, { }, next) => {

  const db: Knex = fastify.db;

  fastify.post('/allergy', { preHandler: [fastify.serverAccess, fastify.authenticate] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const cid = req.body.cid;
    try {
      let vs: any = await _model.drugAllergy(db, cid);
      if (!resultNot0) {
        if (vs.rowCount > 0) {
          vs = vs.rows;
        } else {
          vs = vs[0];// get result for data
        }
      }
      if (vs === undefined || vs.length == 0) {
        reply.code(200).send({ ok: false, message: 'ไม่พบข้อมูล' });
        return ;
      }

      reply.code(200).send({ ok: true, rows: vs })
    } catch (error) {
      fastify.log.error(error);
      reply.code(503).send({ ok: false, message: error })
    }
  });

  fastify.post('/warfarin', { preHandler: [fastify.serverAccess, fastify.authenticate] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const cid = req.body.cid;
    try {
      let vs: any = await _model.drugByCheck(db, cid, process.env.WR_FIELD, process.env.WR_ICODE, process.env.WR_DAYS);
      if (!resultNot0) {
        if (vs.rowCount > 0) {
          vs = vs.rows;
        } else {
          vs = vs[0];// get result for data
        }
      }
      if (vs === undefined || vs.length == 0) {
        reply.code(200).send({ ok: false, message: 'ไม่พบข้อมูล' });
        return ;
      }

      reply.code(200).send({ ok: true, rows: vs })
    } catch (error) {
      fastify.log.error(error);
      reply.code(503).send({ ok: false, message: error })
    }
  });

  fastify.post('/streptokinase', { preHandler: [fastify.serverAccess, fastify.authenticate] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const cid = req.body.cid;
    try {
      let vs: any = await _model.drugByCheck(db, cid, process.env.SK_FIELD, process.env.SK_ICODE, process.env.SK_DAYS);
      if (!resultNot0) {
        if (vs.rowCount > 0) {
          vs = vs.rows;
        } else {
          vs = vs[0];// get result for data
        }
      }
      if (vs === undefined || vs.length == 0) {
        reply.code(200).send({ ok: false, message: 'ไม่พบข้อมูล' });
        return ;
      }

      reply.code(200).send({ ok: true, rows: vs })
    } catch (error) {
      fastify.log.error(error);
      reply.code(503).send({ ok: false, message: error })
    }
  });

  next();

};

module.exports = router;
