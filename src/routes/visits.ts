/// <reference path="../../typings.d.ts" />

import * as Knex from 'knex';
import * as fastify from 'fastify';
import * as HttpStatus from 'http-status-codes';
import * as fs from 'fs';
import {His_model} from '../interfaces/his_model';
import {His_function} from '../interfaces/his_function';

const _model = new His_model().getModel();
const resultNot0 = new His_function().getResultNot0();

const router = (fastify, { }, next) => {

  const db: Knex = fastify.db;

  fastify.post('/', { preHandler: [fastify.serverAccess, fastify.authenticate] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const cid = req.body.cid;
    try {
      let vs: any = await _model.visitAll(db, cid);

      if (!resultNot0) {
        if (vs.rowCount > 0) {
          vs = vs.rows;
        } else {
          vs = vs[0];// get result for data
        }
      }

      if (vs === undefined || vs.length == 0) {
        reply.code(200).send({ ok: false, message: 'ไม่พบข้อมูล' });
        return ;
      }

      reply.code(200).send({ ok: true, rows: vs })
    } catch (error) {
      fastify.log.error(error);
      reply.code(503).send({ ok: false, message: error })
    }
  });

  fastify.post('/register', { preHandler: [fastify.serverAccess, fastify.authenticate] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const cid = req.body.cid;
    try {
      let vs: any = await _model.countService(db, cid);
      if (!resultNot0) {
        if (vs.rowCount > 0) {
          vs = vs.rows;
        } else {
          vs = vs[0];// get result for data
        }
        if (vs === undefined || vs.length == 0) {
          reply.code(200).send({ok: false, message: 'ไม่พบข้อมูล'});
          return;
        }
      }

      vs = vs[0];

      reply.code(200).send({ ok: true, vn: vs.count_vn })
    } catch (error) {
      fastify.log.error(error);
      reply.code(503).send({ ok: false, message: error })
    }
  });

  fastify.get('/:vn', { preHandler: [fastify.serverAccess, fastify.authenticate] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const vn = req.params.vn;
    try {
      let scr: any = await _model.visitScreening(db, vn);
      if (!resultNot0) {
        if (scr.rowCount > 0) {
          scr = scr.rows;
        } else {
          scr = scr[0];// get result for data
        }
      }

      let diag = await _model.visitDiag(db, vn);
      if (!resultNot0) {
        if (diag.rowCount > 0) {
          diag = diag.rows;
        } else {
          diag = diag[0];// get result for data
        }
      }

      let drug = await _model.drugByVn(db, vn);
      if (!resultNot0) {
        if (drug.rowCount > 0) {
          drug = drug.rows;
        } else {
          drug = drug[0];// get result for data
        }
      }

      let lab = await _model.labToDayResultAll(db, vn);
      if (!resultNot0) {
        if (lab.rowCount > 0) {
          lab = lab.rows;
        } else {
          lab = lab[0];// get result for data
        }
      }

      let proc = await _model.visitProcesure(db, vn);
      if (!resultNot0) {
        if (proc.rowCount > 0) {
          proc = proc.rows;
        } else {
          proc = proc[0];// get result for data
        }
      }

      let appoint = await _model.visitAppointment(db, vn);
      if (!resultNot0) {
        if (appoint.rowCount > 0) {
          appoint = appoint.rows;
        } else {
          appoint = appoint[0];// get result for data
        }
      }

      let doctorpdf = await _model.pdfByVn(db, vn);

      reply.code(200).send({ ok: true, screening: scr, diag, drug, lab, proc, appoint, doctorpdf })
    } catch (error) {
      fastify.log.error(error);
      reply.code(503).send({ ok: false, message: error })
    }
  });

  fastify.get('/pdfscan/:vn', { preHandler: [fastify.serverAccess] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const vn = req.params.vn;
    if (vn) {
      try {
        const stream = fs.readFileSync(`./public/pdf22/${vn}.pdf`);
        await reply.type('application/pdf').send(stream);
      } catch (error) {
        fastify.log.error(error);
        reply.code(200).send({ ok: false, message: error.message })
      }
    } else {
      reply.code(200).send({ ok: false, message: 'ข้อมูลไม่ครบ' })
    }

  });

  fastify.get('/pdfscan-base64/:vn', { preHandler: [fastify.serverAccess, fastify.authenticate] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const vn = req.params.vn;
    if (vn) {
      try {
        const stream = fs.readFileSync(`./public/pdf22/${vn}.pdf`, {encoding: 'base64'});
        await reply.type('text/pdf').send(stream);
      } catch (error) {
        fastify.log.error(error);
        reply.code(200).send({ ok: false, message: error.message })
      }
    } else {
      reply.code(200).send({ ok: false, message: 'ข้อมูลไม่ครบ' })
    }

  });

  next();

};

module.exports = router;
