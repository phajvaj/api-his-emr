/// <reference path="../../typings.d.ts" />

import * as Knex from 'knex';
import * as fastify from 'fastify';
import * as crypto from 'crypto';
import * as HttpStatus from 'http-status-codes';
import { UserModel } from '../models/user';

const userModel = new UserModel();

const router = (fastify, { }, next) => {

  var db: Knex = fastify.db;

  fastify.post('/', { preHandler: [fastify.serverAccess] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const body: any = req.body;

    const username = body.username;
    const password = process.env.PASSWORD_KEY + body.password;
    const encPassword = crypto.createHash('md5').update(password).digest('hex');

    try {
      const rs: any = await userModel.login(db, username, encPassword);
      if (!rs.length) {
        reply.code(200).send({ statusCode: 200, message: 'ชื่อผู้ใช้งานหรือรหัสผ่าน ไม่ถูกต้อง' })
      } else {
        const info = rs[0];

        const token = fastify.jwt.sign({
          fullname: info.fullname,
          userId: info.user_id,
          userType: info.user_type,
          GLOBAL_NOTIFY_TOPIC: process.env.GLOBAL_NOTIFY_TOPIC,
          QUEUE_CENTER_TOPIC: process.env.QUEUE_CENTER_TOPIC,
          SERVICE_POINT_TOPIC: process.env.SERVICE_POINT_TOPIC,
          DEPARTMENT_TOPIC: process.env.DEPARTMENT_TOPIC,
          GROUP_TOPIC: process.env.GROUP_TOPIC,
          NOTIFY_USER: process.env.LOCAL_NOTIFY_USER,
          NOTIFY_PASSWORD: process.env.LOCAL_NOTIFY_PASSWORD,
          NOTIFY_SERVER: process.env.LOCAL_NOTIFY_SERVER,
          NOTIFY_PORT: process.env.LOCAL_NOTIFY_HTTP_PORT,
          SPEAK_SINGLE: process.env.SPEAK_SINGLE || 'N'
        }, { expiresIn: '1d' });
        reply.code(200).send({
          ok: true, token: token, notify: info.is_notify
          //servicePoints: rsPoints
        });
      }
    } catch (error) {
      fastify.log.error(error);
      reply.code(503).send({ statusCode: 503, message: error })
    }
  });

  next();

};

module.exports = router;
