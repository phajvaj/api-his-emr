/// <reference path="../../typings.d.ts" />

import * as Knex from 'knex';
import * as fastify from 'fastify';
import * as HttpStatus from 'http-status-codes';
import {His_model} from '../interfaces/his_model';
import {His_function} from '../interfaces/his_function';

const resultNot0 = new His_function().getResultNot0();
const _model = new His_model().getModel();

const router = (fastify, { }, next) => {

  const db: Knex = fastify.db;

  fastify.post('/', { preHandler: [fastify.serverAccess, fastify.authenticate] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const cid = req.body.cid;
    try {
      let vs: any = await _model.xrayLists(db, cid);
      if (!resultNot0) {
        if (vs.rowCount > 0) {
          vs = vs.rows;
        } else {
          vs = vs[0];// get result for data
        }
      }
      if (vs === undefined || vs.length == 0) {
        reply.code(200).send({ ok: false, message: 'ไม่พบข้อมูล' });
        return ;
      }
      reply.code(200).send({ ok: true, rows: vs })
    } catch (error) {
      fastify.log.error(error);
      reply.code(503).send({ ok: false, message: error })
    }
  });

  fastify.get('/result/:idx', { preHandler: [fastify.serverAccess, fastify.authenticate] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const idx = req.params.idx;
    try {
      let rs: any = await _model.xrayResult(db, idx);
      if (!resultNot0) {
        if (rs.rowCount > 0) {
          rs = rs.rows;
        } else {
          rs = rs[0];// get result for data
        }
      }
      if (rs === undefined || rs.length == 0) {
        reply.code(200).send({ ok: false, message: 'ไม่พบข้อมูล' });
        return ;
      }
      reply.code(200).send({ ok: true, rows: rs })
    } catch (error) {
      fastify.log.error(error);
      reply.code(503).send({ ok: false, message: error })
    }

  });

  next();

};

module.exports = router;
