/// <reference path="../../typings.d.ts" />

import * as Knex from 'knex';
import * as fastify from 'fastify';
import * as HttpStatus from 'http-status-codes';
import * as Random from 'random-js';
import moment = require('moment');

import {His_model} from '../interfaces/his_model';

const _model = new His_model().getModel();

const router = (fastify, { }, next) => {

  const db: Knex = fastify.db;

  fastify.post('/', { preHandler: [fastify.serverAccess] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const hosxp = req.body.hosxp;
    if (hosxp !== process.env.HOSP_CODE) {
      reply.code(200).send({ ok: false, error: 'hosp code not found!' })
    }
    let rs = await _model.getHospital(db);
    rs = rs[0];
    const createdDate = moment().format('YYYY-MM-DD HH:mm:ss');
    const expiredDate = moment().add(process.env.EXPIRE_TOKEN, 'year').format('YYYY-MM-DD HH:mm:ss');
    const token = fastify.jwt.sign({
      issue: 'API',
      description: 'token access by api',
      hosp: rs.hospcode,
      hospname: rs.hospname,
    }, { expiresIn: process.env.EXPIRE_TOKEN + 'y' });

    const data: any = {
      token: token,
      created: createdDate,
      expired: expiredDate
    };

    try {
      reply.code(200).send({ ok: true , rows: data })
    } catch (error) {
      fastify.log.error(error);
      reply.code(503).send({ ok: false, error: error })
    }
  });

  next();

};

module.exports = router;
