/// <reference path="../../typings.d.ts" />

import * as Knex from 'knex';
import * as fastify from 'fastify';
import moment = require('moment');
import * as HttpStatus from 'http-status-codes';
import {His_model} from '../interfaces/his_model';
import {His_function} from '../interfaces/his_function';

const _model = new His_model().getModel();
const resultNot0 = new His_function().getResultNot0();

const router = (fastify, { }, next) => {

  const db: Knex = fastify.db;

  fastify.get('/', async (req: fastify.Request, reply: fastify.Reply) => {
    const createdDate = moment().format('YYYY-MM-DD HH:mm:ss');
    reply.code(200).send({ message: 'APIs Client v.1.650311-1511', ip: req.ip, ips: req.ips, host: req.hostname, time: createdDate });
  });

  fastify.get('/info', { preHandler: [fastify.serverAccess] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const createdDate = moment().format('YYYY-MM-DD HH:mm:ss');
    const expiredDate = moment().add(process.env.EXPIRE_TOKEN, 'year').format('YYYY-MM-DD HH:mm:ss');
    const token = fastify.jwt.sign({
      issue: 'PHINGOSOFT',
      description: 'for access iot api',
    }, { expiresIn: process.env.EXPIRE_TOKEN + 'y' });

    try {
      reply.code(200).send({ ok: true })
    } catch (error) {
      console.log(error);
      reply.code(503).send({ ok: false, message: error })
    }
  });

  fastify.get('/pateint', async (req: fastify.Request, reply: fastify.Reply) => {
    try {
      let vs: any = await _model.getPatient(db);

      if (!resultNot0) {
        if (vs.rowCount > 0) {
          vs = vs.rows;
        } else {
          vs = vs[0];// get result for data
        }
      }

      if (vs === undefined || vs.length == 0) {
        reply.code(200).send({ ok: false, message: 'ไม่พบข้อมูล' });
        return ;
      }

      reply.code(200).send({ ok: true, rows: vs })
    } catch (error) {
      fastify.log.error(error);
      reply.code(503).send({ ok: false, message: error })
    }
  });

  next();

};

module.exports = router;
