/// <reference path="../typings.d.ts" />
'use strict';

import path = require('path');
import * as HttpStatus from 'http-status-codes';
import * as fastify from 'fastify';
import * as multer  from 'fastify-multer';
import Knex = require('knex');
import { Server, IncomingMessage, ServerResponse } from 'http';

import helmet = require('fastify-helmet');

import * as moment from 'moment';
import router from './router';

const pgTypes = require('pg').types;
const serveStatic = require('serve-static');

const swaggerConfig = require('../swagger_config');


require('dotenv').config({ path: path.join(__dirname, '../config') });

const app: fastify.FastifyInstance<Server, IncomingMessage, ServerResponse> = fastify({
  logger: {
    level: 'error',
    prettyPrint: true
  },
  bodyLimit: 5 * 1048576,
  trustProxy: true,
});

app.register(require('fastify-swagger'), swaggerConfig.options);
app.register(require('fastify-formbody'));
app.register(require('fastify-cors'), {});
app.register(require('fastify-no-icon'));
app.register(multer.contentParser);

app.register(require('fastify-static'), {
  root: path.join(__dirname, '../public'),
  //prefix: '/public/', // optional: default '/'
});

app.register(
  helmet,
  { hidePoweredBy: { setTo: 'PHP 5.2.0' } }
);

app.register(require('fastify-rate-limit'), {
  global : false,
  max: +process.env.MAX_CONNECTION_PER_MINUTE || 3000,
  timeWindow: '1.5 minute',
  skipOnError: true,
  cache: 10000,
});

app.use(serveStatic(path.join(__dirname, '../public')));

app.register(require('fastify-jwt'), {
  secret: process.env.SECRET_KEY
});

let templateDir = path.join(__dirname, '../templates');
app.register(require('point-of-view'), {
  engine: {
    ejs: require('ejs')
  },
  templates: templateDir
});
/*
app.addHook('onRequest', (req, reply, done) => {
  try {
    if(req.ip !== '127.0.0.1'){
      reply.status(HttpStatus.BAD_GATEWAY).send({
        ok: false,
        message: 'Not request for gateway'
      })
    }

    if(process.env.SERVER_ACCESS){//'49.229.87.103'
      if(req.ip !== process.env.SERVER_ACCESS){
        reply.status(HttpStatus.BAD_GATEWAY).send({
          ok: false,
          message: 'Not request for gateway'
        })
      }
    }

  } catch (err) {

  }

  done()
});
*/
app.decorate('serverAccess', async (req, reply) => {
  try {
    if(process.env.SERVER_GATEWAY_IP){//'49.229.87.103'
      if(req.ip !== process.env.SERVER_GATEWAY_IP && req.ips[0] !== process.env.SERVER_GATEWAY_IP){
        reply.status(HttpStatus.BAD_GATEWAY).send({
          ok: false,
          ip: req.ip, ips: req.ips,
          message: 'Not request for server gateway'
        })
      }
    }

  } catch (err) {
    reply.status(HttpStatus.BAD_REQUEST).send({
      ok: false,
      message: err
    })
  }

});

app.decorate("authenticate", async (request, reply) => {
  let token = null;

  try {
    if (request.headers.authorization && request.headers.authorization.split(' ')[0] === 'Bearer') {
      token = request.headers.authorization.split(' ')[1];
    } else if (request.query && request.query.token) {
      token = request.query.token;
    } else {
      token = request.body.token;
    }
  } catch {

  }


  try {
    const decoded = await request.jwtVerify(token);
  } catch (err) {
    reply.status(HttpStatus.UNAUTHORIZED).send({
      ok: false,
      error: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED),
      message: '401 UNAUTHORIZED!'
    })
  }
});

// Database
let dbConnection: Knex.MySqlConnectionConfig = {
  host: process.env.DB_HOST,
  port: +process.env.DB_PORT,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  typeCast: function (field, next) {
    if (field.type.toUpperCase() === 'DATETIME') {
      const dt = moment(field.string()).format('YYYY-MM-DD HH:mm:ss');
      return dt === 'Invalid date' ? null : dt;
    }
    if (field.type.toUpperCase() === 'DATE') {
      const dt = moment(field.string()).format('YYYY-MM-DD');
      return dt === 'Invalid date' ? null : dt;
    }
    return next();
  }
};

let knexConfig: any;
if (process.env.DB_CLIENT === 'mssql' || process.env.DB_CLIENT == 'oracledb') {
  knexConfig = {
    client: process.env.DB_CLIENT,
    connection: dbConnection,
    fetchAsString: ['date'],
  };
} else if (process.env.DB_CLIENT === 'pg') {
  const parseDt = function(val) {
    return val === null ? null : moment(val).format('YYYY-MM-DD');
  };
  const parseFn = function(val) {
    return val === null ? null : moment(val).format('YYYY-MM-DD HH:mm:ss');
  };
  pgTypes.setTypeParser(pgTypes.builtins.DATE, parseDt);
  pgTypes.setTypeParser(pgTypes.builtins.TIMESTAMPTZ, parseFn);
  pgTypes.setTypeParser(pgTypes.builtins.TIMESTAMP, parseFn);

  knexConfig = {
    client: process.env.DB_CLIENT,
    searchPath: ['knex', 'public'],
    connection: dbConnection,
  };
} else {
  knexConfig = {
    client: process.env.DB_CLIENT,
    connection: dbConnection,
    pool: {
      min: 0,
      max: 7,
      afterCreate: (conn, done) => {
        conn.query('SET NAMES utf8', (err) => {
          done(err, conn);
        });
      }
    },
    debug: false,
    acquireConnectionTimeout: 5000
  };
}

app.register(require('./plugins/db'), {
  connection: knexConfig,
  connectionName: 'db'
});

/*
app.register(require('./plugins/db'), {
  connection: {
    client: process.env.DB_CLIENT,
    connection: {
      host: process.env.DB_HOST,
      user: process.env.DB_USER,
      port: +process.env.DB_PORT,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      typeCast: function (field, next) {
        if (field.type == 'DATETIME') {
          return moment(field.string()).format('YYYY-MM-DD HH:mm:ss');
        }
        if (field.type == 'DATE') {
          return moment(field.string()).format('YYYY-MM-DD');
        }
        return next();
      }
    },
    pool: {
      min: 0,
      max: 7,
      afterCreate: (conn, done) => {
        conn.query('SET NAMES utf8', (err) => {
          done(err, conn);
        });
      }
    },
    debug: false,
  },
  connectionName: 'db'
});
*/

// MQTT
/*app.register(require('./plugins/mqtt'), {
  host: process.env.MQTT_SERVER,
  port: process.env.MQTT_PORT,
  username: process.env.MQTT_USER,
  password: process.env.MQTT_PASSWORD
});*/

// Cron job
/*app.register(require('./plugins/crons_job'), {
  job_call: process.env.JOB_CALL_API,
  job_notify: process.env.JOB_SEND_NOTIFY,
  iot_center: process.env.IOT_CENTER_TOPIC,
  iot_service: process.env.SERVICE_POINT_TOPIC,
  iot_group: process.env.GROUP_TOPIC,
  line_token: process.env.LINE_TOKEN,
});*/

app.decorate('verifyAdmin', function (request, reply, done) {
  if (request.user.userType === 'ADMIN') {
    done();
  } else {
    reply.status(HttpStatus.UNAUTHORIZED).send({ ok: false, message: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED) });
  }
});

app.decorate('verifyMember', function (request, reply, done) {
  if (request.user.userType === 'MEMBER') {
    done();
  } else {
    reply.status(HttpStatus.UNAUTHORIZED).send({ ok: false, message: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED) });
  }
});

app.decorate('verifyAPI', function (request, reply, done) {
  if (request.user.issue === 'API' && request.user.HOSxP === process.env.HOSP_CODE) {
    done();
  } else {
    reply.status(HttpStatus.UNAUTHORIZED).send({ ok: false, message: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED) });
  }
});

app.register(router);

const port = +process.env.API_PORT || 3000;
const host = '0.0.0.0';

app.listen(port, host, (err) => {
  if (err) throw err;
  // @ts-ignore
  //app.swagger();
  console.log(app.server.address());
});
