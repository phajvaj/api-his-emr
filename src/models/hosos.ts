import * as knex from 'knex';
import * as util from 'util';

export class HososModel {

  getHospital(db: knex) {
    return db('b_site')
        .select('b_visit_office_id as hospcode', 'site_name as hospname')
        .limit(1);
  }

  countService(db: knex, cid: any) {
    const sql = `SELECT COUNT(v.vn) as count_vn FROM ovst as v
    INNER JOIN patient as p ON(v.hn=p.hn) WHERE p.cid='${cid}'`;
    return db.raw(sql);
  }

  visitAll(db: knex, cid: any) {
    const sql = `select 
    t_visit.visit_vn as vn
    ,case when t_visit.f_visit_type_id='1' and t_visit.f_emergency_status_id='0' then 'IPD'
    when t_visit.f_visit_type_id='0' and t_visit.f_emergency_status_id='0' then 'OPD'
      when t_visit.f_emergency_status_id !='0' then 'ER'
    else 'OPD' end as spclty
    ,case when t_visit.f_visit_type_id!='1' then ''  else t_visit.visit_vn end as an
    ,t2edate(t_visit.visit_begin_visit_time) as vstdate
    ,right(t_visit.visit_begin_visit_time,8) as vsttime
    from t_visit
    INNER JOIN t_patient on t_patient.t_patient_id = t_visit.t_patient_id
    INNER JOIN t_diag_icd10 on t_diag_icd10.diag_icd10_vn = t_visit.t_visit_id  and t_diag_icd10.diag_icd10_active='1'
              and (left(t_diag_icd10.diag_icd10_number,3) not in ('(Z21','R75','Y05') or t_diag_icd10.diag_icd10_number not between 'B20.0' and 'B24.9')
    WHERE t_patient.patient_pid ='${cid}' and t_visit.f_visit_status_id<>'4' and t_visit.f_visit_type_id='0'
    ORDER BY t2edate(t_visit.visit_begin_visit_time) desc;`;
    return db.raw(sql);
  }

  visitScreening(db: knex, vn: any) {
    const sql = `SELECT 
    SPLIT_PART(t_visit_vital_sign.visit_vital_sign_blood_presure, '/',1) as bps
    ,SPLIT_PART(t_visit_vital_sign.visit_vital_sign_blood_presure, '/',2) as bpd
    ,t_visit_vital_sign.visit_vital_sign_weight as weight
    ,t_visit_vital_sign.visit_vital_sign_height as height
    ,t_visit_vital_sign.visit_vital_sign_bmi as bmi
    ,t_visit_vital_sign.visit_vital_sign_waistline_inch as waist
    ,t_visit_vital_sign.visit_vital_sign_temperature as temperature
    ,t_visit_vital_sign.visit_vital_sign_respiratory_rate as rr
    ,null as hr
    ,t_visit_vital_sign.visit_vital_sign_heart_rate as pulse
    ,t_visit_primary_symptom.visit_primary_symptom_main_symptom as cc
    ,t_visit_primary_symptom.visit_primary_symptom_current_illness as pe
    ,null as hpi
    ,null as pmh
    ,null as fh
    ,null as sh
    ,t_visit_primary_symptom.visit_primary_symptom_main_symptom as symptom
    ,null as smoking
    ,null as drinking
    
    from t_visit_vital_sign
    left JOIN t_visit on t_visit.t_visit_id = t_visit_vital_sign.t_visit_id and t_visit.f_visit_type_id='0' -- OPD
    INNER JOIN t_patient on t_patient.t_patient_id = t_visit.t_patient_id
    left join t_visit_primary_symptom on t_visit_primary_symptom.t_visit_id = t_visit.t_visit_id
    WHERE t_visit.visit_vn = '${vn}'; `;
    return db.raw(sql);
  }

  visitDiag(db: knex, vn: any) {
    const sql = `select 
    t_diag_icd10.diag_icd10_number as icd10
    ,t_diag_icd10.f_diag_icd10_type_id as diagtype
    ,b_icd10.icd10_description as icd10name
    ,f_diag_icd10_type.diag_icd10_type_description as diagtypename 
    
    from t_visit
    
    INNER JOIN t_diag_icd10 on t_diag_icd10.diag_icd10_vn = t_visit.t_visit_id
    INNER JOIN t_patient on t_patient.t_patient_id = t_visit.t_patient_id
    left join f_diag_icd10_type on t_diag_icd10.f_diag_icd10_type_id = f_diag_icd10_type.f_diag_icd10_type_id
    left join b_icd10 on b_icd10.icd10_number = t_diag_icd10.diag_icd10_number
    WHERE t_visit.f_visit_type_id='0' and t_visit.visit_vn = '${vn}' 
    ORDER BY t_diag_icd10.f_diag_icd10_type_id;`;
    return db.raw(sql);
  }

  visitProcesure(db: knex, vn: any) {
    const sql = `select 
    t_diag_icd9.diag_icd9_icd9_number as icd9
    ,b_icd9.icd9_description as opername
    ,null as result
    from t_visit
    INNER JOIN t_diag_icd9 on t_diag_icd9.diag_icd9_vn = t_visit.t_visit_id
    left join b_icd9 on b_icd9.icd9_number = t_diag_icd9.diag_icd9_icd9_number
    WHERE t_visit.f_visit_type_id='0'
     and t_visit.visit_vn = '${vn}'`;
    return db.raw(sql);
  }

  visitAppointment(db: knex, vn: any) {
    const sql = `select 
    t2edate(t_patient_appointment.patient_appointment_date)  as nextdate
    ,t_patient_appointment.patient_appointment_time as nexttime
    ,b_visit_clinic.visit_clinic_description as clinic
    ,b_employee.employee_firstname||' '||b_employee.employee_firstname as contact
    ,t_patient_appointment.patient_appointment as note
    ,t_patient_appointment.patient_appointment_notice as note1
    ,null as note2
    from t_patient_appointment
    INNER JOIN t_patient on t_patient.t_patient_id = t_patient_appointment.t_patient_id
    left join t_visit on t_visit.t_visit_id = t_patient_appointment.t_visit_id
    LEFT JOIN b_visit_clinic on b_visit_clinic.b_visit_clinic_id = t_patient_appointment.patient_appointment_clinic
    LEFT JOIN b_employee on b_employee.b_employee_id = t_patient_appointment.patient_appointment_doctor
    WHERE
    t_patient_appointment.patient_appointment_active='1'
    and t2edate(t_patient_appointment.patient_appointment_date)::timestamp >=now()
    AND t_visit.visit_vn = '${vn}'
    and left(t_patient_appointment.patient_appointment_date,4)>='2564'
    ORDER BY t2edate(t_patient_appointment.patient_appoint_date_time) desc ;`;
    return db.raw(sql);
  }

  biopsyLists(db: knex, cid: any) {
    return [{indx: '', itm: '', vstdate: ''}];
  }

  biopsyResult(db: knex, idx: any) {
    return [{result: ''}];
  }

  // Drugs

  drugAllergy(db: knex, cid: any) {
    const sql = `select 
    b_item_drug_standard.item_drug_standard_description as agent
    ,'' as agentcode24
    ,t_patient_drug_allergy.drug_allergy_symtom as symptom
    ,t_patient_drug_allergy.drug_allergy_note as note
    ,t2edate(t_patient_drug_allergy.drug_allergy_symtom_date) as begindate
    ,t2edate(t_patient_drug_allergy.drug_allergy_report_date) as reportdate
    from t_patient_drug_allergy
    INNER JOIN b_item_drug_standard on b_item_drug_standard.b_item_drug_standard_id = t_patient_drug_allergy.b_item_drug_standard_id
    inner join t_patient on t_patient.t_patient_id = t_patient_drug_allergy.t_patient_id
    INNER JOIN b_employee on b_employee.b_employee_id = t_patient_drug_allergy.pharma_assess_id
    where t_patient_drug_allergy.active='1'
    and t_patient.patient_pid = '${cid}';`;
    return db.raw(sql);
  }

  drugByVn(db: knex, vn: any) {
    const sql = `SELECT 
    case when length(trim(b_item.item_general_number))=24 then b_item.item_general_number 
      else substr(b_nhso_map_drug.f_nhso_drug_id,1,24) end as did
    ,b_item.item_common_name as "name"
    ,t_order.order_qty as qty
    ,case when b_item_drug_instruction.item_drug_instruction_description is null then ''
                else b_item_drug_instruction.item_drug_instruction_description||' '||
                        t_order_drug.order_drug_dose||' '||uom_use.item_drug_uom_description end ||' '||
    case when b_item_drug_frequency.item_drug_frequency_description is null then '' else b_item_drug_frequency.item_drug_frequency_description end as shortlist 
    ,b_employee.employee_firstname||' '||b_employee.employee_lastname as provider
    
    from t_order
    INNER JOIN t_order_drug on t_order_drug.t_order_id = t_order.t_order_id and t_order_drug.order_drug_active = '1'
    inner JOIN t_visit on t_visit.t_visit_id = t_order.t_visit_id and t_visit.f_visit_type_id='0' -- OPD
    INNER JOIN b_item on b_item.b_item_id = t_order.b_item_id
    left join b_nhso_map_drug on b_nhso_map_drug.b_item_id = t_order.b_item_id
    left join b_employee on b_employee.b_employee_id = t_order.order_staff_order
    left join b_item_drug_frequency on t_order_drug.b_item_drug_frequency_id = b_item_drug_frequency.b_item_drug_frequency_id
    left join b_item_drug_instruction on t_order_drug.b_item_drug_instruction_id = b_item_drug_instruction.b_item_drug_instruction_id
    left join b_item_drug_uom as uom_use on t_order_drug.b_item_drug_uom_id_use = uom_use.b_item_drug_uom_id
    left join b_item_drug_uom as uom_purch on t_order_drug.b_item_drug_uom_id_purch = uom_purch.b_item_drug_uom_id
    
    
    WHERE t_visit.visit_vn = '${vn}' and t_visit.f_visit_status_id<>'4'
     and t_order.f_order_status_id not in ('0','3')
     and t_order.f_item_group_id='1' ; `;
    return db.raw(sql);
  }

  drugByCheck(db: knex, cid: any, field: any, did: any, days: any) {
    return [{service: '', drugname: '', qty: '', unit: '', strength: '', vstdate: '', dt: 40}];
  }

  //Labs

  labLists(db: knex, cid: any) {
    const sql = `SELECT h.lab_order_number as labordernumber,h.vn,h.order_date as orderdate,h.order_time as ordertime,h.report_date as reportdate,
    h.report_time as reporttime,h.form_name as formname,
    IFNULL(h.receive_date, h.order_date) as receivedate,h.receive_time as receivetime,h.department
    FROM lab_head as h
    INNER JOIN lab_order as o ON(h.lab_order_number=o.lab_order_number )
    INNER JOIN patient as p ON(h.hn=p.hn)
    WHERE p.cid='${cid}' AND h.confirm_report='Y' 
    AND o.lab_items_code NOT IN(SELECT lab_items_code FROM lab_items WHERE lab_items_name LIKE '%hiv%')
    AND h.vn NOT IN(SELECT a.vn FROM ovstdiag as a INNER JOIN patient as b ON(a.hn=b.hn) 
    WHERE b.cid='${cid}' AND a.icd10 BETWEEN 'B200' AND 'B249' GROUP BY a.vn)
    GROUP BY h.lab_order_number ORDER BY h.order_date DESC,h.order_time`;
    return db.raw(sql);
  }

  labToDayResultAll(db: knex, vn: any) {
    const sql = `SELECT 
    b_item.item_number AS labcode
    ,b_item.item_common_name AS labname
    ,t_result_lab.result_lab_value AS result
    ,t_result_lab.result_lab_critical_min||'-'||t_result_lab.result_lab_critical_max as ref
    ,b_lab_result_group.lab_result_group_name as remark
    ,t_result_lab.result_lab_staff_record as staff
    from t_order
    inner JOIN t_visit on t_visit.t_visit_id = t_order.t_visit_id and t_visit.f_visit_type_id='0'
    INNER JOIN t_result_lab on t_order.t_order_id = t_result_lab.t_order_id and t_result_lab.result_lab_active='1'
    INNER JOIN b_item on b_item.b_item_id = t_order.b_item_id
    LEFT JOIN b_lab_result_group on b_lab_result_group.b_lab_result_group_id = t_result_lab.b_lab_result_group_id
    WHERE
    t_visit.visit_vn = '${vn}'
    and t_visit.f_visit_status_id<>'4'
    and t_order.f_order_status_id not in ('0','3')
    and t_order.f_item_group_id='2'
    order by t_visit.visit_begin_visit_time desc; `;
    return db.raw(sql);
  }

  labByOrdernumber(db: knex, orn: any) {
    const sql = `SELECT o.lab_items_code as labcode,o.lab_items_name_ref as labname,o.lab_order_result as result,i.lab_items_unit as unit,
    i.lab_items_normal_value as ref,o.lab_order_remark as remark,o.staff
    FROM lab_order as o 
    INNER JOIN lab_items as i ON(o.lab_items_code=i.lab_items_code)
    WHERE o.lab_order_number='${orn}' AND o.confirm='Y' AND o.lab_order_result<>''
    AND o.lab_items_code NOT IN(SELECT lab_items_code FROM lab_items WHERE lab_items_name LIKE '%hiv%')`;
    return db.raw(sql);
  }

  //X-ray

  xrayLists(db: knex, cid: any) {
    return [{vstdate: '', icode: '', name: '', xstatus: ''}];
  }

  xrayResult(db: knex, idx: any) {
    return [{result: ''}];
  }

  //pdf list

  pdfByVn(db: knex, vn: any) {
    return [];
  }
}
