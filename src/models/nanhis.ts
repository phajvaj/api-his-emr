import * as knex from 'knex';
import * as util from 'util';

export class NanhisModel {

  getHospital(db: knex) {
    return db('hisconfig')
        .select('hospcode', 'hospname').limit(1);
  }

  countService(db: knex, cid: any) {
    const sql = `SELECT COUNT(v.hn) as count_vn FROM visit as v
    INNER JOIN patient as p ON(v.hn=p.hn)
    WHERE p.person_code='${cid}' AND v.\`out\` NOT IN('0', '9')`;
    return db.raw(sql);
  }

  visitAll(db: knex, cid: any) {
    const sql = `SELECT v.code_visit as vn,v.hn,date(v.day_arr) as vstdate,time(v.time) as vsttime,
    a.an,s.name_service as spclty
    FROM visit as v
    INNER JOIN patient as p ON(v.hn=p.hn)
    LEFT JOIN service as s ON(v.service=s.code_service)
    LEFT JOIN admit as a ON(v.code_visit=a.visit_code)
    WHERE p.person_code='${cid}' AND v.\`out\` NOT IN('0', '9') AND v.service<>'112'
    GROUP BY vn ORDER BY vn DESC`;
    return db.raw(sql);
  }

  visitScreening(db: knex, vn: any) {
    const sql = `SELECT v.bp_u as bpd,v.bp_l as bps,v.weight as bw,v.tall as height,CalBMI(v.weight, v.tall) as bmi,0 as waist,v.pulse,v.tempe as temperature,
    v.resp as rr,v.detail_pat as cc,p.his_diag as hr,null as pe,p.txta3 as hpi,p.txta2 as pmh,null as fh,p.othera as sh,null as symptom,IF(v.smoke='0', 'ไม่สูบ', IF(v.smoke='1', 'สูบ', IF(v.smoke='2', 'เลิกสูบ', 'คนในบ้านสูบ'))) as smoking,IF(v.drunk='0', 'ไม่ดื่ม', 'ดื่ม') as drinking
    FROM hosp_growpat as v 
    LEFT JOIN hosp_pat as p ON(v.hn=p.hn)
    WHERE v.visit_code='${vn}'`;
    return db.raw(sql);
  }

  visitDiag(db: knex, vn: any) {
    const sql = `SELECT d.diag as icd10,d.dxtype as diagtype,i.name_tm as icd10name,t.name as diagtypename
    FROM ODXyymm_n as d 
    LEFT JOIN icd10_tm as i ON(d.diag=i.icd10_tm)
    LEFT JOIN diagtype as t ON(d.dxtype=t.id)
    WHERE d.visit_code='${vn}'
    ORDER BY diagtype`;
    return db.raw(sql);
  }

  visitProcesure(db: knex, vn: any) {
    const sql = `SELECT IFNULL(i.name_tm,c.\`name\`) as opername FROM OOPyymm_n as o
    LEFT JOIN icd9_tm as i ON(o.oper=i.icd9_cm or o.icd9_tm=i.icd9_tm)
    LEFT JOIN icd9_cm as c ON(o.oper=c.\`code\`)
    WHERE o.visit_code='${vn}'`;
    return db.raw(sql);
  }

  visitAppointment(db: knex, vn: any) {
    const sql = `SELECT b.book_date as nextdate,b.book_time as nexttime,s.name_serv as clinic,
    null as contact,null as note,null as note1,null
    as note2
    FROM opd_book as b
    LEFT JOIN service_code as s ON(b.book_serv=s.code_serv)
    WHERE b.book_visit='${vn}'`;
    return db.raw(sql);
  }

  biopsyLists(db: knex, cid: any) {
    const sql = `select b.indx,b.itm,b.biopsyno,date(b.date_rep) as vstdate from biopsy as b
    inner join patient as p on(b.hn=p.hn) 
    where p.person_code='${cid}' order by vstdate desc,b.itm;`;
    return db.raw(sql);
  }

  biopsyResult(db: knex, idx: any) {
    const sql = `select pic as result from biopsy where indx='${idx}';`;
    return db.raw(sql);
  }

  // Drugs

  drugAllergy(db: knex, cid: any) {
    const sql = `SELECT IFNULL(i.drugthai,i.drugname) as agent,i.codepharm as agentcode24,d.remark as symptom,d.note,
    d.date_adr as begindate,d.date_adr as reportdate
    FROM adr as d
    INNER JOIN patient as p ON(d.hn=p.hn)
    INNER JOIN inventory_ipd as i ON(d.drugcode=i.drugcode)
    WHERE p.person_code='${cid}' AND d.orduse<>"ยกเลิก"`;
    return db.raw(sql);
  }

  drugByVn(db: knex, vn: any) {
    const sql = `SELECT CONCAT(p.drugname, ' ', p.useunit) as \`name\`,p.quant as qty,
    CONCAT(if(p.drgucmmt <> '', p.drgucmmt, p.drug_use), ' ',if(p.drgucmmt <> '', p.drug_use, '')) as shortlist
    FROM prsclist_opd as p
    LEFT JOIN inventory_ipd as i ON(p.drugcode=i.drugcode)
    WHERE p.visit_code = '${vn}' AND p.grouptype LIKE 'ZZZ%'`;
    return db.raw(sql);
  }

  drugByCheck(db: knex, cid: any, field: any, did: any, days: any) {
    const sql = `SELECT 'op' as service,p.drugname as drugname,p.quant as qty,p.useunit as unit,'' as strength,p.date as vstdate,DATEDIFF(CURDATE(), p.date)  as dt
    FROM prsclist_opd as p
    INNER JOIN patient as t ON(p.hn=t.hn)
    LEFT JOIN inventory_ipd as i ON(p.drugcode=i.drugcode)
    WHERE p.grouptype LIKE 'ZZZ%' 
    AND i.${field} LIKE '${did}' 
    AND p.visit_code<>'' AND t.person_code='${cid}'
    AND p.quant > 0
    AND DATEDIFF(CURDATE(), p.date) <= ${days}
    ORDER BY p.date DESC LIMIT 1`;
    return db.raw(sql);
  }

  //Labs

  labLists(db: knex, cid: any) {
    const sql = `SELECT a.code_ord as labordernumber,a.code_visit as vn,a.day_ord as orderdate,
    TIME_FORMAT(a.time_ord,'%H:%i:%s') as ordertime,IF(a.day_rep='0000-00-00', a.day_ord, a.day_rep) as reportdate,
    TIME_FORMAT(a.time_rep,'%H:%i:%s') as reporttime,b.name as formname,
    IF(a.day_rec='0000-00-00', a.day_ord, a.day_rec) as receivedate,TIME_FORMAT(a.time_rec,'%H:%i:%s') as receivetime,IF(a.an<>'', 'IPD','OPD') as department
    FROM orde_lab as a
    INNER JOIN lab_out as o ON(a.code_ord=o.orde_code AND o.value_1<>'')
    INNER JOIN lab_list as b ON(a.code_lab=b.code_inv)
    INNER JOIN patient as p ON(a.hn=p.hn)
    WHERE p.person_code='${cid}' AND a.resuit='T' AND a.service<>'112' ORDER BY orderdate DESC,ordertime`;
    return db.raw(sql);
  }

  async labToDayResultAll(db: knex, vn: any) {
    const sql = `SELECT a.code_ord as labcode,
    c.lab_name as labname,c.result as result,c.note as ref,'' as remark,null as staff
    FROM orde_lab a INNER JOIN lab_list b ON(a.code_lab = b.code_inv)
    LEFT JOIN
    (
    SELECT l1.orde_code,l2.word_1 as lab_name,l1.value_1 as result,l3.normal_1 as note, 1 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_visit=? AND l1.value_1<>'' UNION ALL
    SELECT l1.orde_code,l2.word_2 as lab_name,l1.value_2 as result,l3.normal_2 as note, 2 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_visit=? AND l1.value_2<>'' UNION ALL
    SELECT l1.orde_code,l2.word_3 as lab_name,l1.value_3 as result,l3.normal_3 as note, 3 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_visit=? AND l1.value_3<>'' UNION ALL
    SELECT l1.orde_code,l2.word_4 as lab_name,l1.value_4 as result,l3.normal_4 as note, 4 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_visit=? AND l1.value_4<>'' UNION ALL
    SELECT l1.orde_code,l2.word_5 as lab_name,l1.value_5 as result,l3.normal_5 as note, 5 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_visit=? AND l1.value_5<>'' UNION ALL
    SELECT l1.orde_code,l2.word_6 as lab_name,l1.value_6 as result,l3.normal_6 as note, 6 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_visit=? AND l1.value_6<>'' UNION ALL
    SELECT l1.orde_code,l2.word_7 as lab_name,l1.value_7 as result,l3.normal_7 as note, 7 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_visit=? AND l1.value_7<>'' UNION ALL
    SELECT l1.orde_code,l2.word_8 as lab_name,l1.value_8 as result,l3.normal_8 as note, 8 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_visit=? AND l1.value_8<>'' UNION ALL
    SELECT l1.orde_code,l2.word_9 as lab_name,l1.value_9 as result,l3.normal_9 as note, 9 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_visit=? AND l1.value_9<>'' UNION ALL
    SELECT l1.orde_code,l2.word_10 as lab_name,l1.value_10 as result,l3.normal_10 as note, 10 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_visit=? AND l1.value_10<>'' UNION ALL
    SELECT l1.orde_code,l2.word_11 as lab_name,l1.value_11 as result,l3.normal_11 as note, 11 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_visit=? AND l1.value_11<>'' UNION ALL
    SELECT l1.orde_code,l2.word_12 as lab_name,l1.value_12 as result,l3.normal_12 as note, 12 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_visit=? AND l1.value_12<>'' UNION ALL
    SELECT l1.orde_code,l2.word_13 as lab_name,l1.value_13 as result,l3.normal_13 as note, 13 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_visit=? AND l1.value_13<>'' UNION ALL
    SELECT l1.orde_code,l2.word_14 as lab_name,l1.value_14 as result,l3.normal_14 as note, 14 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_visit=? AND l1.value_14<>'' UNION ALL
    SELECT l1.orde_code,l2.word_15 as lab_name,l1.value_15 as result,l3.normal_15 as note, 15 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_visit=? AND l1.value_15<>''
    ) as c ON(a.code_ord=c.orde_code)
    LEFT JOIN
    (
    SELECT a.code_ord,b.memo_text,b.reported,b.approved FROM orde_lab as a
    INNER JOIN lab_out_memo as b ON(a.code_ord=b.orde_code)
    WHERE a.code_visit=?
    GROUP BY a.code_ord
    ) as d ON(a.code_ord=d.code_ord)
    WHERE a.code_visit=? AND (c.result<>'' OR d.memo_text<>'') AND a.service<>'112'
    ORDER BY a.code_ord,c.labno
    `;
    const data = await db.raw(sql, [vn, vn, vn, vn, vn, vn, vn, vn, vn, vn, vn, vn, vn, vn, vn, vn, vn]);
    return data;
    }

  async labByOrdernumber(db: knex, orn: any) {
      const sql = `SELECT a.code_ord as labcode,
    c.lab_name as labname,c.result as result,c.note as ref,'' as remark,null as staff
    FROM orde_lab a INNER JOIN lab_list b ON(a.code_lab = b.code_inv)
    LEFT JOIN
    (
    SELECT l1.orde_code,l2.word_1 as lab_name,l1.value_1 as result,l3.normal_1 as note, 1 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_ord=? AND l1.value_1<>'' UNION ALL
    SELECT l1.orde_code,l2.word_2 as lab_name,l1.value_2 as result,l3.normal_2 as note, 2 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_ord=? AND l1.value_2<>'' UNION ALL
    SELECT l1.orde_code,l2.word_3 as lab_name,l1.value_3 as result,l3.normal_3 as note, 3 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_ord=? AND l1.value_3<>'' UNION ALL
    SELECT l1.orde_code,l2.word_4 as lab_name,l1.value_4 as result,l3.normal_4 as note, 4 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_ord=? AND l1.value_4<>'' UNION ALL
    SELECT l1.orde_code,l2.word_5 as lab_name,l1.value_5 as result,l3.normal_5 as note, 5 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_ord=? AND l1.value_5<>'' UNION ALL
    SELECT l1.orde_code,l2.word_6 as lab_name,l1.value_6 as result,l3.normal_6 as note, 6 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_ord=? AND l1.value_6<>'' UNION ALL
    SELECT l1.orde_code,l2.word_7 as lab_name,l1.value_7 as result,l3.normal_7 as note, 7 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_ord=? AND l1.value_7<>'' UNION ALL
    SELECT l1.orde_code,l2.word_8 as lab_name,l1.value_8 as result,l3.normal_8 as note, 8 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_ord=? AND l1.value_8<>'' UNION ALL
    SELECT l1.orde_code,l2.word_9 as lab_name,l1.value_9 as result,l3.normal_9 as note, 9 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_ord=? AND l1.value_9<>'' UNION ALL
    SELECT l1.orde_code,l2.word_10 as lab_name,l1.value_10 as result,l3.normal_10 as note, 10 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_ord=? AND l1.value_10<>'' UNION ALL
    SELECT l1.orde_code,l2.word_11 as lab_name,l1.value_11 as result,l3.normal_11 as note, 11 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_ord=? AND l1.value_11<>'' UNION ALL
    SELECT l1.orde_code,l2.word_12 as lab_name,l1.value_12 as result,l3.normal_12 as note, 12 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_ord=? AND l1.value_12<>'' UNION ALL
    SELECT l1.orde_code,l2.word_13 as lab_name,l1.value_13 as result,l3.normal_13 as note, 13 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_ord=? AND l1.value_13<>'' UNION ALL
    SELECT l1.orde_code,l2.word_14 as lab_name,l1.value_14 as result,l3.normal_14 as note, 14 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_ord=? AND l1.value_14<>'' UNION ALL
    SELECT l1.orde_code,l2.word_15 as lab_name,l1.value_15 as result,l3.normal_15 as note, 15 as labno
    FROM orde_lab a INNER JOIN lab_out as l1 ON(a.code_ord=l1.orde_code) LEFT JOIN lab_template as l2 ON(l1.code_inv=l2.code_inv) LEFT JOIN lab_normal as l3 ON(l1.code_inv=l3.code_inv) WHERE a.code_ord=? AND l1.value_15<>''
    ) as c ON(a.code_ord=c.orde_code)
    LEFT JOIN
    (
    SELECT a.code_ord,b.memo_text,b.reported,b.approved FROM orde_lab as a
    INNER JOIN lab_out_memo as b ON(a.code_ord=b.orde_code)
    WHERE a.code_visit=?
    GROUP BY a.code_ord
    ) as d ON(a.code_ord=d.code_ord)
    WHERE a.code_ord=? AND (c.result<>'' OR d.memo_text<>'') AND a.service<>'112'
    ORDER BY a.code_ord,c.labno
    `;
    const data = await db.raw(sql, [orn, orn, orn, orn, orn, orn, orn, orn, orn, orn, orn, orn, orn, orn, orn, orn, orn]);
    return data;
  }

  //X-ray

  xrayLists(db: knex, cid: any) {
    const sql = `select date(o.date_ord) as vstdate,o.code_ord as icode,x.name,
    case when o.status = '1' then 'รายงานโดยแพทย์รังสี'
    when o.status = '2' then 'บันทึกผล แต่ยังไม่รายงาน'
    when o.status = '3' then 'รายงานผลอย่างไม่ทางการ'
    else 'ยังไม่ถูกรายงาน' end xstatus
    from orde_xray o 
    inner join xray_list x on (o.code_xray=x.code_inv)
    inner join patient as p on(o.hn=p.hn)
    where p.person_code='${cid}' and x.name not like 'ผลชิ้นเนื้อ%' and pn<>'xxxxxxxx' 
    and o.type<>'0' order by vstdate desc;`;
    return db.raw(sql);
  }

  xrayResult(db: knex, idx: any) {
    const sql = `select word as result from xray_out_new 
    where code_orde='${idx}' and usety in ('T','E','A');`;
    return db.raw(sql);
  }

  //pdf list

  pdfByVn(db: knex, vn: any) {
    const sql = db(`hosp_pdf`)
        .select('indx as id', 'visit_code as vn', db.raw('date(date_save) as vstdate'), 'diag', 'remark');

    sql.where('visit_code', vn);
    sql.orderBy('filenum', 'desc');

    return sql;
  }
}
