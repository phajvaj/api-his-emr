import * as knex from 'knex';
import * as util from 'util';

export class SsbModel {

  subVN (vn) {
    return {dt: vn.substr(0, 8), vn: vn.substr(8)}
  }

  async getHospital(db: knex) {
    const his = process.env.HOSP_CODE;
    let data = await db.raw(`SELECT CODE as hospcode,right(THAINAME,LEN(THAINAME)-1) as hospname from SYSCONFIG WHERE CTRLCODE='20010' and CODE='${his}'`);
    return data;
  }

  countService(db: knex, cid: any) {
    const sql = `SELECT COUNT(vm.VN) as count_vn 
    FROM VNMST vm WITH (NOLOCK)
    LEFT JOIN PATIENT_REF pr WITH (NOLOCK)
    ON vm.HN = pr.HN AND pr.REFTYPE = '01' WHERE pr.REF = '${cid}' `;
    return db.raw(sql);
  }

  visitAll(db: knex, cid: any) {
    const sql = `SELECT CONVERT(VARCHAR(10), vm.VISITDATE,112) + VM.VN AS vn,NULL AS an,  
      CASE
        WHEN VP.VN IS NOT NULL
    THEN 'ER' 
        ELSE dbo.fnGetSysConfig_ThEn_WithOutCode(VPN.CLINIC,'20016')
      END AS spclty,
      CONVERT(VARCHAR(10), vm.VISITINDATETIME,120) AS vstdate,
      SUBSTRING(CONVERT(VARCHAR, vm.VISITINDATETIME, 108),0,6) AS vsttime,
      CONVERT(VARCHAR(10), VM.VISITOUTDATETIME,120) AS dchdate,
      SUBSTRING(CONVERT(VARCHAR, VM.VISITOUTDATETIME, 108),0,6) AS dchtime
    FROM VNMST vm WITH (NOLOCK)
    LEFT JOIN PATIENT_REF pr WITH (NOLOCK)
    ON vm.HN = pr.HN AND pr.REFTYPE = '01' 
    LEFT OUTER JOIN VNPRES VPN WITH (NOLOCK)
    ON VPN.VN = VM.VN AND VPN.VISITDATE = VM.VISITDATE
    LEFT OUTER JOIN VNPRES VP WITH (NOLOCK)
    ON VP.VN = VM.VN AND VP.VISITDATE = VM.VISITDATE
    AND 
    CONVERT(VARCHAR(10),VP.VISITDATE,112) + CONVERT(VARCHAR,VP.VN) = 
    (
    SELECT TOP 1 
    CONVERT(VARCHAR(10),VNPRES.VISITDATE,112) + CONVERT(VARCHAR,VNPRES.VN) 
    FROM VNPRES WITH (NOLOCK) 
    LEFT OUTER JOIN SERVER_EXTENSION.dbEXTENSION.dbo.DCRMW_tbReport_CloseVisitCanceled with (nolock)
    ON DCRMW_tbReport_CloseVisitCanceled.CloseVisitType = VNPRES.CLOSEVISITTYPE
    WHERE 
    VNPRES.VISITDATE = VM.VISITDATE AND VNPRES.VN = VM.VN AND VNPRES.CLINIC LIKE N'10%'
    AND DCRMW_tbReport_CloseVisitCanceled.CloseVisitType IS NULL
    ) 
    LEFT OUTER JOIN SERVER_EXTENSION.dbEXTENSION.dbo.DCRMW_tbReport_CloseVisitCanceled with (nolock)
    ON DCRMW_tbReport_CloseVisitCanceled.CloseVisitType = VP.CLOSEVISITTYPE
    WHERE pr.REF = '${cid}' 
    AND DCRMW_tbReport_CloseVisitCanceled.CloseVisitType IS NULL`;
    return db.raw(sql);
  }

  visitScreening(db: knex, vn: any) {
    const rs = this.subVN(vn);
    const sql = `SELECT CONVERT(INT, vm.BPHIGH) AS bpd,CONVERT(INT, vm.BPLOW) AS bps,vm.BODYWEIGHT AS bw,
    vm.HEIGHT as height,ROUND(vm.BODYWEIGHT / POWER(vm.HEIGHT / 100, 2),2) AS bmi,
    null as waist,CONVERT(INT, vm.TEMPERATURE) AS temperature,CONVERT(INT, vm.TEXTRESPIRERATE) AS rr,
    null AS cc,null AS hr,null AS pe,null AS pulse,null AS hpi,null AS pmh,null AS fh,null AS sh,
    null AS symptom,NULL AS smoking,NULL AS drinking
    FROM VNMST vm WITH (NOLOCK)
    WHERE CONVERT(VARCHAR(10),vm.VISITDATE,112) = '${rs.dt}' AND vm.VN = '${rs.vn}'`;
    return db.raw(sql);
  }

  visitDiag(db: knex, vn: any) {
    const rs = this.subVN(vn);
    const sql = `SELECT 
    VNDIAG.ICDCODE as icd10,VNDIAG.TYPEOFTHISDIAG as diagtype
    ,dbo.fnGetICD10_EnTh(VNDIAG.ICDCODE) as icd10name
    ,case when VNDIAG.TYPEOFTHISDIAG = '1' then 'Primary'
    when VNDIAG.TYPEOFTHISDIAG = '2' then 'Complication'
    when VNDIAG.TYPEOFTHISDIAG = '3' then 'Other'
    when VNDIAG.TYPEOFTHISDIAG = '4' then 'Comorbidity'
    else '' end as diagtypename
    From VNDIAG WITH (NOLOCK)
    where CONVERT(VARCHAR(10),VNDIAG.VISITDATE,112) = '${rs.dt}'
    AND CONVERT(VARCHAR,VNDIAG.VN) = '${rs.vn}'
    order by VNDIAG.TYPEOFTHISDIAG`;
    return db.raw(sql);
  }

  visitProcesure(db: knex, vn: any) {
    const rs = this.subVN(vn);
    const sql = `SELECT DIAGCODE as icd9,dbo.fnGetICD9_EnTh(DIAGCODE) as icd9name,null as result
    FROM VNMST vm WITH (NOLOCK) 
    LEFT OUTER JOIN VNPRES VP WITH (NOLOCK)
    ON VP.VN = VM.VN AND VP.VISITDATE = VM.VISITDATE
    LEFT OUTER JOIN SERVER_EXTENSION.dbEXTENSION.dbo.DCRMW_tbReport_CloseVisitCanceled with (nolock)
    ON DCRMW_tbReport_CloseVisitCanceled.CloseVisitType = VP.CLOSEVISITTYPE
    INNER JOIN 
    (SELECT VN,VISITDATE,SUFFIX,DIAGCODE
    FROM(SELECT VNMST.VN,VNMST.VISITDATE,SUFFIX,PROCUDUREICDCMCODE1,PROCUDUREICDCMCODE2,
    PROCUDUREICDCMCODE3,PROCUDUREICDCMCODE4
    FROM VNMST WITH (NOLOCK)
    INNER JOIN VNDIAG WITH (NOLOCK)
    ON VNMST.VN = VNDIAG.VN AND VNMST.VISITDATE = VNDIAG.VISITDATE
    WHERE CONVERT(VARCHAR(10),VNMST.VISITDATE,112) = '${rs.dt}'
    AND CONVERT(VARCHAR,VNMST.VN) = '${rs.vn}'
    )PIVOTTABLE
    UNPIVOT (DIAGCODE FOR DIAGCOLS IN (PROCUDUREICDCMCODE1,PROCUDUREICDCMCODE2,
    PROCUDUREICDCMCODE3,PROCUDUREICDCMCODE4)) AS UNPVT
    GROUP BY VN,VISITDATE,SUFFIX, DIAGCODE
    ) VNDX ON VM.VN = VNDX.VN AND VM.VISITDATE = VNDX.VISITDATE AND VNDX.SUFFIX = VP.SUFFIX
    WHERE CONVERT(VARCHAR(10),vm.VISITDATE,112) = '${rs.dt}'
    AND CONVERT(VARCHAR,vm.VN) = '${rs.vn}' 
    AND DCRMW_tbReport_CloseVisitCanceled.CloseVisitType IS NULL`;
    return db.raw(sql);
  }

  visitAppointment(db: knex, vn: any) {
    const rs = this.subVN(vn);
    const sql = `select  
    convert(date,APPOINTMENTDATETIME) as nextdate
    ,convert(time(0),APPOINTMENTDATETIME) as nexttime
    ,dbo.fnGetSysConfig_ThEn_WithOutCode(HNAPPMNT.APPOINTMENTWITHCLINIC,'20016') as clinic
    ,dbo.fnGetPrename(PATIENT_NAME.HN)+' '+ substring(PATIENT_NAME.FIRSTNAME,2,len(PATIENT_NAME.FIRSTNAME)) + '  ' + substring(PATIENT_NAME.LASTNAME,2,len(PATIENT_NAME.LASTNAME)) + ' ' + PATIENT_ADDRESS.TEL as contact
    ,dbo.fnGetSysConfig_ThEn_WithOutCode(HNAPPMNT.PROCEDURECODE,'20109') AS note 
    ,dbo.fnGetSysConfig_ThEn_WithOutCode(HNAPPMNT.PROCEDURECODE2,'20109') AS note1
    ,dbo.fnGetSysConfig_ThEn_WithOutCode(HNAPPMNT.PROCEDURECODE3,'20109') AS note2
    from VNMST VM WITH (NOLOCK)
    INNER JOIN HNAPPMNT WITH (NOLOCK)
    ON CONVERT(VARCHAR, VM.VISITDATE,112) = CONVERT(VARCHAR, HNAPPMNT.MAKEDATETIME,112)AND VM.HN = HNAPPMNT.HN
    inner join PATIENT_NAME WITH (NOLOCK)
    on HNAPPMNT.HN = PATIENT_NAME.HN and PATIENT_NAME.SUFFIX = 0
    inner join PATIENT_ADDRESS WITH (NOLOCK)
    on HNAPPMNT.HN = PATIENT_ADDRESS.HN and PATIENT_ADDRESS.SUFFIX = 1
    WHERE 
    CONVERT(VARCHAR(10),VM.VISITDATE,112) = '${rs.dt}'
    AND CONVERT(VARCHAR,VM.VN) = '${rs.vn}'
    AND APPOINTMENTDATETIME IS NOT NULL`;
    return db.raw(sql);
  }

  biopsyLists(db: knex, cid: any) {
    return [{indx: '', itm: '', vstdate: ''}];
  }

  biopsyResult(db: knex, idx: any) {
    return [{result: ''}];
  }

  // Drugs

  drugAllergy(db: knex, cid: any) {
    const sql = `select dbo.fnGetStockMaster_EnTh_WithOutCode(PATIENT_ALLERGIC.MEDICINE) as agent,
    dbo.fnGetSysConfig_ThEn_WithOutCode(PATIENT_ALLERGIC.ADVERSEREACTIONS1,'20028') as symptom,
    null as note,null as begin_date,null as report_date
    from PATIENT_ALLERGIC WITH (NOLOCK)
    LEFT JOIN PATIENT_REF pr WITH (NOLOCK)
    ON PATIENT_ALLERGIC.HN = pr.HN AND REFTYPE = '01'
    where pr.REF = '${cid}'`;
    return db.raw(sql);
  }

  drugByVn(db: knex, vn: any) {
    const rs = this.subVN(vn);
    const sql = `select dbo.fnGetStockMaster_EnTh_WithOutCode(dr.STOCKCODE) as name
    ,convert(varchar,dr.QTY) as qty
    ,dbo.fnGetSysConfig_ThEn_WithOutCode(dr.DOSETYPE,'20031') + ' ' +
    dbo.fnGetSysConfig_ThEn_WithOutCode(dr.DOSEQTYCODE,'20033') + ' ' +
    dbo.fnGetSysConfig_ThEn_WithOutCode(dr.DOSEUNITCODE,'20034') + ' ' +
    dbo.fnGetSysConfig_ThEn_WithOutCode(dr.DOSECODE,'20032') as shortlist
    from VNMEDICINE dr with (nolock)
    where CONVERT(VARCHAR(10),VISITDATE,112) = '${rs.dt}'
    AND CONVERT(VARCHAR,VN) = '${rs.vn}'
    and CXLBYUSERCODE is null`;
    return db.raw(sql);
  }

  drugByCheck(db: knex, cid: any, field: any, did: any, days: any) {
    const sql = `SELECT top 1 VDR.STOCKCODE AS drugcode,
    dbo.fnGetStockMaster_EnTh_WithOutCode(VDR.STOCKCODE) as  drugname,
    CONVERT(VARCHAR(10), vm.VISITINDATETIME,120) AS vstdate,
    convert(varchar,VDR.QTY)  as qty,
    dbo.fnGetSysConfig_ThEn_WithOutCode(VDR.UNITCODE,'20034') as units,
    NULL AS strength,
    DATEDIFF(day,getdate(), vm.VISITINDATETIME) AS dt 
    from VNMST VM WITH (NOLOCK)
    INNER JOIN VNMEDICINE VDR with (nolock)
    ON VDR.VN = VM.VN AND VDR.VISITDATE = VM.VISITDATE
    inner join Drug with (nolock)
    on VDR.STOCKCODE = Drug.HOSPDRUGCODE and Drug.NDC24 LIKE N'${did}' 
    INNER JOIN PATIENT_REF pr WITH (NOLOCK)
    ON vm.HN = pr.HN AND pr.REFTYPE = '01' 
    where pr.REF = '${cid}'
    and DATEDIFF(day,getdate(), vm.VISITINDATETIME) <= ${days} and CXLBYUSERCODE is null`;
    return db.raw(sql);
  }

  //Labs

  labLists(db: knex, cid: any) {
    const sql = `SELECT rq.REQUESTNO as labordernumber,rq.REQUESTFROMVN AS vn,
    CONVERT(VARCHAR(10), rq.ENTRYDATETIME,120)  as orderdate,
    SUBSTRING(CONVERT(VARCHAR, rq.ENTRYDATETIME, 108),0,6) as ordertime,
    CONVERT(VARCHAR(10), rq.RESULTREADYDATETIME,120)  as reportdate,
    SUBSTRING(CONVERT(VARCHAR, rq.RESULTREADYDATETIME, 108),0,6) as reporttime,
    ISNULL(STUFF((
         SELECT ', ' + dbo.fnGetSysConfig_EnTh_WithOutCode(rs.LABREQUESTMETHOD, '20068' )
         FROM LABRESULT rs with (nolock)
         WHERE 
         (ls.REQUESTNO = rs.REQUESTNO) 
         AND rs.LABREQUESTMETHOD IS NOT NULL
         GROUP BY rs.LABREQUESTMETHOD
         FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
         ,1,2,'') ,'')
         +' | '+
         ISNULL(STUFF((
         SELECT ', ' + dbo.fnGetSysConfig_EnTh_WithOutCode(rs.LABCODE, '20067' )
         FROM LABRESULT rs with (nolock)
         WHERE 
         (ls.REQUESTNO = rs.REQUESTNO) 
         GROUP BY rs.LABCODE
     FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
             ,1,2,''),'') AS formname,
    CONVERT(VARCHAR(10), rq.RECEIVESPECIMENDATETIME,120) as receivedate,
    SUBSTRING(CONVERT(VARCHAR, rq.RECEIVESPECIMENDATETIME, 108),0,6) as receivetime,
    rq.CLINIC AS department
    from LABREQ rq with (nolock)
    INNER JOIN PATIENT_REF pr WITH (NOLOCK)
    ON rq.HN = pr.HN AND pr.REFTYPE = '01'
    LEFT OUTER JOIN LABRESULT ls WITH (NOLOCK)
    ON ls.REQUESTNO = rq.REQUESTNO
    where pr.REF = '${cid}' and rq.CXLBYUSERCODE is null         
    group by rq.REQUESTNO,rq.REQUESTFROMVN,rq.ENTRYDATETIME,
    rq.RESULTREADYDATETIME,rq.RECEIVESPECIMENDATETIME,rq.CLINIC,ls.REQUESTNO`;
    return db.raw(sql);
  }

  labToDayResultAll(db: knex, vn: any) {
    const rs = this.subVN(vn);
    const sql = `SELECT LABCODE as labcode,dbo.fnGetSysConfig_EnTh_WithOutCode(ls.LABCODE,'20067') as labname
    ,RESULTVALUE as result,dbo.fnGetSysConfig_EnTh_WithOutCode(ls.RESULTUNITCODE,'20071') as unit
    ,NORMALRESULTVALUE as ref,dbo.fnGetSysConfig_ThEn_WithOutCode(ls.RESULTBYUSERCODE,'10000') as staff
    from LABREQ req with (nolock)
    inner join LABRESULT ls with (nolock) 
    on req.FACILITYRMSNO = ls.FACILITYRMSNO and req.REQUESTNO = ls.REQUESTNO
    where RESULTVALUE is not null and ls.HIDERESULT = 0
    and CONVERT(VARCHAR(10),req.CHARGETOVISITDATE,112) = '${rs.dt}'
    AND CONVERT(VARCHAR,req.CHARGETOVN) = '${rs.vn}'
    and ls.CXLDATETIME is null`;
    return db.raw(sql);
  }

  labByOrdernumber(db: knex, orn: any) {
    const sql = `select LABCODE as labcode,dbo.fnGetSysConfig_EnTh_WithOutCode(ls.LABCODE,'20067') as labname
    ,RESULTVALUE as result,dbo.fnGetSysConfig_EnTh_WithOutCode(ls.RESULTUNITCODE,'20071') as unit
    ,NORMALRESULTVALUE as ref,'' as remark
    ,dbo.fnGetSysConfig_ThEn_WithOutCode(ls.RESULTBYUSERCODE,'10000') as staff
    from LABREQ as req with (nolock)
    inner join LABRESULT ls with (nolock) 
    on req.FACILITYRMSNO = ls.FACILITYRMSNO and req.REQUESTNO = ls.REQUESTNO
    WHERE RESULTVALUE is not null 
    and ls.HIDERESULT = 0 and req.REQUESTNO = '${orn}' and ls.CXLDATETIME is null`;
    return db.raw(sql);
  }

  //X-ray

  xrayLists(db: knex, cid: any) {
    return [{vstdate: '', icode: '', name: '', xstatus: ''}];
  }

  xrayResult(db: knex, idx: any) {
    return [{result: ''}];
  }

  //pdf list

  pdfByVn(db: knex, vn: any) {
    return [];
  }
}
