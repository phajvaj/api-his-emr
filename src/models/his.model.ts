import * as knex from 'knex';

export class HisModel {

  getHospital(db: knex) {
    return [{hospcode: '00000', hospname: 'hospital'}];
  }

  countService(db: knex, cid: any) {
    return [{count_vn: 0}];
  }

  visitAll(db: knex, cid: any) {
    return [{vn: '', hn: '', vstdate: '', vsttime: '', an: '', spclty: ''}];
  }

  visitScreening(db: knex, vn: any) {
    return [{bpd: '', bps: '', bw: '', height: '', bmi: '', waist: '', pulse: '', temperature: '',
    rr: '', cc: '', hr: '',pe: '', hpi: '', pmh: '', fh: '', sh: '', symptom: '', smoking: '', drinking: ''}];
  }

  visitDiag(db: knex, vn: any) {
    return [{icd10: '', diagtype: '', icd10name: '', diagtypename: ''}];
  }

  visitProcesure(db: knex, vn: any) {
    return [{opername: ''}];
  }

  visitAppointment(db: knex, vn: any) {
    return [{nextdate: '', nexttime: '', clinic: '', contact: '', note: '', note1: '', note2: ''}];
  }

  biopsyLists(db: knex, cid: any) {
    return [{indx: '', itm: '', vstdate: ''}];
  }

  biopsyResult(db: knex, idx: any) {
    return [{result: ''}];
  }

  // Drugs

  drugAllergy(db: knex, cid: any) {
    return [{agent: '', agentcode24: '', symptom: '', note: '', begindate: '', reportdate: ''}];
  }

  drugByVn(db: knex, vn: any) {
    return [{name: '', qty: '', shortlist: ''}];
  }

  drugByCheck(db: knex, cid: any, field: any, did: any, days: any) {
    return [{service: '', drugname: '', qty: '', unit: '', strength: '', vstdate: '', dt: 40}];
  }

  //Labs

  labLists(db: knex, cid: any) {
    return [{labordernumber: '', vn: '', orderdate: '', ordertime: '', reportdate: '', reporttime: '', formname: '',
      receivedate: '', receivetime: '', department: ''}];
  }

  async labToDayResultAll(db: knex, vn: any) {
    return [{labcode: '', labname: '', result: '', ref: '', remark: '', staff: ''}];
    }

  async labByOrdernumber(db: knex, orn: any) {
    return [{labcode: '', labname: '', result: '', ref: '', remark: '', staff: ''}];
  }

  //X-ray

  xrayLists(db: knex, cid: any) {
    return [{vstdate: '', icode: '', name: '', xstatus: ''}];
  }

  xrayResult(db: knex, idx: any) {
    return [{result: ''}];
  }

  //pdf list

  pdfByVn(db: knex, vn: any) {
    return [{id: '', vn: '', vstdate: '', diag: '', remark: ''}];
  }
}
