import * as knex from 'knex';
import * as util from 'util';

export class Hosxpv4pgModel {

  getHospital(db: knex) {
    return db('opdconfig as o')
        .select('o.hospitalcode as hospcode', 'o.hospitalname as hospname')
        .limit(1);
  }

  countService(db: knex, cid: any) {
    const sql = `SELECT COUNT(v.vn) as count_vn FROM ovst as v
    INNER JOIN patient as p ON(v.hn=p.hn) WHERE p.cid='${cid}'`;
    return db.raw(sql);
  }

  visitAll(db: knex, cid: any) {
    const sql = `SELECT v.vn,v.hn,v.vstdate,v.vsttime,v.an,s.name as spclty  
    FROM ovst as v INNER JOIN patient as p ON(v.hn=p.hn)
    LEFT JOIN spclty as s ON(v.spclty=s.spclty)
    WHERE p.cid='${cid}' AND v.vn NOT IN(SELECT a.vn FROM ovstdiag as a INNER JOIN patient as b ON(a.hn=b.hn) 
    WHERE b.cid='${cid}' AND a.icd10 BETWEEN 'B200' AND 'B249' GROUP BY a.vn) 
    GROUP BY v.vn,v.hn,v.vstdate,v.vsttime,v.an,s.name ORDER BY v.vstdate DESC;`;
    // let [rs] = await Promise.all([db.raw(sql)]);
    //return rs;
    return db.raw(sql);
  }

  visitScreening(db: knex, vn: any) {
    const sql = `select o.bpd,o.bps,o.bw,o.height,o.bmi,o.waist,o.temperature,o.rr,o.cc,o.hr,o.pe,o.pulse,o.hpi,o.pmh,o.fh,o.sh,o.symptom,
    s.smoking_type_name as smoking,d.drinking_type_name as drinking
    from opdscreen as o
    left join smoking_type as s on(o.smoking_type_id=s.smoking_type_id)
    left join drinking_type as d on(o.drinking_type_id=d.drinking_type_id)
    where o.vn='${vn}'`;
    return db.raw(sql);
  }

  visitDiag(db: knex, vn: any) {
    const sql = `SELECT v.icd10,v.diagtype,i.name as icd10name,d.name as diagtypename
    FROM ovstdiag as v
    INNER JOIN icd101 as i ON(v.icd10=i.code)
    LEFT JOIN diagtype as d ON(v.diagtype=d.diagtype)
    WHERE v.vn='${vn}' ORDER BY v.diagtype`;
    return db.raw(sql);
  }

  visitProcesure(db: knex, vn: any) {
    const sql = `select o.name as opername
    from er_regist_oper e
    left outer join er_oper_code o on o.er_oper_code=e.er_oper_code
    where e.vn='${vn}'`;
    return db.raw(sql);
  }

  visitAppointment(db: knex, vn: any) {
    const sql = `select o.nextdate,o.nexttime,c.name as clinic,o.contact_point as contact,o.note,o.note1,o.note2 from oapp as o inner join clinic as c on(o.clinic=c.clinic)
    where o.vn='${vn}'`;
    return db.raw(sql);
  }

  biopsyLists(db: knex, cid: any) {
    return [{indx: '', itm: '', vstdate: ''}];
  }

  biopsyResult(db: knex, idx: any) {
    return [{result: ''}];
  }

  // Drugs

  drugAllergy(db: knex, cid: any) {
    const sql = `SELECT a.agent,a.agent_code24 as agentcode24,a.symptom,a.note,a.begin_date as begindate,a.report_date as reportdate 
    FROM opd_allergy as a
      INNER JOIN patient as p ON(a.hn=p.hn) WHERE p.cid='${cid}'`;
    return db.raw(sql);
  }

  drugByVn(db: knex, vn: any) {
    const sql = `select concat(s.name,' ',s.strength,' ',s.units) as name,o.qty,d.shortlist 
    from opitemrece o 
    inner join ovst as v on(o.vn=v.vn or (o.an=v.an and o.item_type='H'))
    left outer join s_drugitems s on(s.icode=o.icode)
    left outer join drugusage d on (d.drugusage=o.drugusage)
    left outer join sp_use u on (u.sp_use = o.sp_use)
    left outer join drugitems i on (i.icode=o.icode) where v.vn='${vn}' and o.icode like '1%'  order by o.item_no`;
    return db.raw(sql);
  }

  drugByCheck(db: knex, cid: any, field: any, did: any, days: any) {
    const sql = `SELECT case when o.vn<>'' then 'op' else 'ip' end as service,d.name as drugname,o.vstdate,o.qty,
    d.units,d.strength,(CURRENT_DATE - o.vstdate) as dt
    FROM opitemrece as o
    INNER JOIN drugitems as d ON(o.icode=d.icode) 
    INNER JOIN patient as p ON(o.hn=p.hn) 
    WHERE d.${field} LIKE '${did}'
    AND p.cid='${cid}'
    AND o.qty > 0
    AND (CURRENT_DATE - o.vstdate) <= ${days}
    ORDER BY o.vstdate DESC LIMIT 1`;
    return db.raw(sql);
  }

  //Labs

  labLists(db: knex, cid: any) {
    const sql = `SELECT h.lab_order_number as labordernumber,h.vn,h.order_date as orderdate,h.order_time as ordertime,h.report_date as reportdate,
    h.report_time as reporttime,h.form_name as formname,
    NULLIF(h.receive_date, h.order_date) as receivedate,h.receive_time as receivetime,h.department
    FROM lab_head as h
    INNER JOIN lab_order as o ON(h.lab_order_number=o.lab_order_number )
    INNER JOIN patient as p ON(h.hn=p.hn)
    WHERE p.cid='${cid}' AND h.confirm_report='Y' 
    AND o.lab_items_code NOT IN(SELECT lab_items_code FROM lab_items WHERE lab_items_name LIKE '%hiv%')
    AND h.vn NOT IN(SELECT a.vn FROM ovstdiag as a INNER JOIN patient as b ON(a.hn=b.hn) 
    WHERE b.cid='${cid}' AND a.icd10 BETWEEN 'B200' AND 'B249' GROUP BY a.vn)
    GROUP BY h.lab_order_number ORDER BY h.order_date DESC,h.order_time`;
    return db.raw(sql);
  }

  labToDayResultAll(db: knex, vn: any) {
    const sql = `SELECT o.lab_items_code as labcode,o.lab_items_name_ref as labname,o.lab_order_result as result,i.lab_items_unit as unit,
    i.lab_items_normal_value as ref,o.lab_order_remark as remark,o.staff
    FROM lab_head as h INNER JOIN ovst as v ON(h.vn=v.vn or h.vn=v.an)
    INNER JOIN lab_order as o  ON(h.lab_order_number=o.lab_order_number)
    LEFT JOIN ovstdiag as d ON(h.vn=d.vn)
    INNER JOIN lab_items as i ON(o.lab_items_code=i.lab_items_code)
    WHERE v.vn='${vn}' AND o.confirm='Y' AND o.lab_order_result<>''
    AND o.lab_items_code NOT IN(SELECT lab_items_code FROM lab_items WHERE lab_items_name LIKE '%hiv%')
    AND d.icd10 NOT BETWEEN 'B200' AND 'B249' 
    GROUP BY h.lab_order_number,labcode,labname,result,unit,ref,remark,o.staff`;
    return db.raw(sql);
  }

  labByOrdernumber(db: knex, orn: any) {
    const sql = `SELECT o.lab_items_code as labcode,o.lab_items_name_ref as labname,o.lab_order_result as result,i.lab_items_unit as unit,
    i.lab_items_normal_value as ref,o.lab_order_remark as remark,o.staff
    FROM lab_order as o 
    INNER JOIN lab_items as i ON(o.lab_items_code=i.lab_items_code)
    WHERE o.lab_order_number='${orn}' AND o.confirm='Y' AND o.lab_order_result<>''
    AND o.lab_items_code NOT IN(SELECT lab_items_code FROM lab_items WHERE lab_items_name LIKE '%hiv%')`;
    return db.raw(sql);
  }

  //X-ray

  xrayLists(db: knex, cid: any) {
    return [{vstdate: '', icode: '', name: '', xstatus: ''}];
  }

  xrayResult(db: knex, idx: any) {
    return [{result: ''}];
  }

  //pdf list

  pdfByVn(db: knex, vn: any) {
    return [];
  }

  getPatient(db: knex) {
    const sql = `SELECT hn,pname,fname,lname,sex,birthday,addrpart,moopart,tmbpart,amppart,chwpart,bloodgrp,clinic,drugallergy,hcode FROM patient LIMIT 10`;
    return db.raw(sql);
  }
}
