import * as knex from 'knex';
import * as util from 'util';

export class SoftconModel {

  getHospital(db: knex) {
    return db('SoftCon.GlobalConfig')
        .select('Value as hospcode', 'THTHText as hospname')
        .where('ConfigCode', 'HospMainNumber')
        .limit(1);
  }

  countService(db: knex, cid: any) {
    const sql = `SELECT COUNT(vs.VN) as count_vn
    FROM Visit AS vs
    LEFT JOIN Person AS ps ON vs.PatientKey = ps.PersonKey
    WHERE ps.Cid = '${cid}'`;
    return db.raw(sql);
  }

  visitAll(db: knex, cid: any) {
    const sql = `SELECT vs.VN as vn,ad.AN as an,CASE
      WHEN ad.AN IS NOT NULL THEN 'IPD' ELSE 'OPD' 
      END AS spclty
     ,FORMAT (vs.DocDT, 'yyyy-MM-dd' , 'en-us') AS vstdate
     ,FORMAT (vs.DocDT, 'HH:mm:ss') AS vsttime
     ,FORMAT (ad.dischargeDT, 'yyyy-MM-dd' , 'en-us') AS dchdate
     ,FORMAT (ad.dischargeDT, 'HH:mm:ss') AS dchtime
    FROM Visit AS vs
    LEFT JOIN Admit AS ad ON vs.VisitKey = ad.VisitKey
    LEFT JOIN Person AS ps ON vs.PatientKey = ps.PersonKey
    WHERE ps.Cid = '${cid}'
    ORDER BY vs.DocDT DESC`;
    return db.raw(sql);
  }

  visitScreening(db: knex, vn: any) {
    const sql = `SELECT vis.VN AS vn,civ_sub.bps,civ_sub.bpd
    ,FORMAT(civ.Weight, 'F3', 'en-us')  AS weight
    ,FORMAT(civ.Height, 'F3', 'en-us')  AS height
    ,FORMAT((civ.Weight/NULLIF(((civ.Height/100)*(civ.Height/100)),0)), 'F3', 'en-us')  AS bmi
    ,FORMAT(civ.WaistLength, 'F3', 'en-us')  AS waist
    ,civ_sub.temperature
    ,civ_sub.rr,NULL AS hr,civ_sub.pulse,cc.Name AS cc,civ.PENote AS pe,civ.PINote AS hpi,civ.PmhNote AS pmh
    ,civ.FshNote AS fh,NULL AS sh,civ.RegNote As symptom,NULL AS smoking,NULL AS drinking
    FROM Visit AS vis
    LEFT JOIN ClinicVisit AS civ ON civ.VisitKey = vis.VisitKey
    LEFT JOIN CC AS cc ON cc.CCKey = civ.CCKey
    OUTER APPLY (
    SELECT TOP 1 
    FORMAT(cvs.SystolicBP, 'F3', 'en-us')  AS bps
    ,FORMAT(cvs.DiastolicBP, 'F3', 'en-us')  AS bpd
    ,FORMAT(cvs.Temp, 'F3', 'en-us')  AS temperature
    ,FORMAT(cvs.RR, 'F3', 'en-us')  AS rr
    ,FORMAT(cvs.PR, 'F3', 'en-us')  AS pulse
     FROM ClinicVisitVS AS cvs 
    WHERE 
    cvs.VisitKey = vis.VisitKey 
    AND cvs.ClinicVisitKey = civ.ClinicVisitKey
    AND cvs.SystolicBP IS NOT NULL 
    AND cvs.DiastolicBP IS NOT NULL 
    ORDER BY cvs.ClinicVisitVSKey DESC 
    ) AS civ_sub 
    LEFT JOIN Person AS pes ON pes.PersonKey = vis.PatientKey
    WHERE 
    civ.CancelDT IS NULL
    AND vis.VN  = '${vn}'
    ORDER BY vis.VN`;
    return db.raw(sql);
  }

  visitDiag(db: knex, vn: any) {
    const sql = `SELECT i.Icd10Code as icd10,
    d.ENName AS icd10name,
    i.DXTypeKey AS diagtypename
    FROM ClinicVisit c INNER JOIN CNDX i ON i.ClinicVisitKey = c.ClinicVisitKey
    INNER JOIN Visit v ON v.VisitKey = c.VisitKey
    INNER JOIN Icd10 d ON d.Icd10Code= i.Icd10Code
    WHERE v.VN = '${vn}' ORDER BY i.DXTypeKey;`;
    return db.raw(sql);
  }

  visitProcesure(db: knex, vn: any) {
    const sql = `SELECT it.Icd9Code as icd9,
    ic.ENName as opername,
    cn.ResultNote AS result
    FROM Visit vs join ClinicVisit cv on vs.VisitKey = cv.VisitKey 
    JOIN CNIssue cn ON cv.ClinicVisitKey = cn.ClinicVisitKey
    JOIN Item it ON cn.ItemKey = it.ItemKey
    JOIN Icd9 ic ON it.Icd9Code = ic.Icd9Code 
    WHERE (cn.CancelDT is null or cn.CancelDT = '') and vs.VN = '${vn}'
    
    UNION 
    
    SELECT it.Icd9Code as icd9,
    ic.ENName as opername,
    an.ResultNote AS result
    FROM Admit am 
    JOIN ANIssue an ON am.AdmitKey = an.AdmitKey 
    JOIN Item it on an.ItemKey = it.ItemKey 
    JOIN Icd9 ic ON it.Icd9Code = ic.Icd9Code
    JOIN Visit vs on am.VisitKey = vs.VisitKey 
    WHERE (an.CancelDT is null or an.CancelDT = '') and vs.VN = '${vn}'`;
    return db.raw(sql);
  }

  visitAppointment(db: knex, vn: any) {
    const sql = `SELECT cv.CN AS cn
    ,FORMAT (ap.FromDT , 'yyyy-MM-dd' , 'en-us') AS nextdate
    ,FORMAT (ap.FromDT , 'HH:mm:ss' , 'en-us') AS nexttime
    ,svu.Name AS clinic
    ,'เคาน์เตอร์เวชระเบียน' AS contact
    ,ap.Note AS note
    ,ap.AppointTopicName AS note1
    ,NULL AS note2 
    
    FROM Appoint AS ap
    LEFT JOIN ServiceUnit AS svu ON ap.ToServiceUnitKey = svu.ServiceUnitKey
    LEFT JOIN ClinicVisit AS cv ON ap.FromClinicVisitKey = cv.ClinicVisitKey
    LEFT JOIN Visit as v ON cv.VisitKey = v.VisitKey 
    WHERE v.VN = '${vn}'`;
    return db.raw(sql);
  }

  biopsyLists(db: knex, cid: any) {
    return [];
  }

  biopsyResult(db: knex, idx: any) {
    return [];
  }

  // Drugs

  drugAllergy(db: knex, cid: any) {
    const sql = `SELECT g.Name AS agent,
    a.Note AS symptom,
    NULL AS agentcode24,NULL AS note,
    a.RecordDT AS begindate,
    a.ApproveDT AS reportdate
    FROM PatientAdr a INNER JOIN DrugGenericName g ON g.DrugGenericNameKey = a.DrugGenericNameKey
    INNER JOIN Patient p ON p.PatientKey = a.PatientKey
    INNER JOIN Person s ON s.PersonKey = p.PatientKey
    WHERE s.Cid = '${cid}'`;
    return db.raw(sql);
  }

  drugByVn(db: knex, vn: any) {
    const sql = `SELECT r.ItemName AS name,r.Qty AS qty,r.UsageNote AS shortlist 
    FROM ClinicVisit c INNER JOIN Visit s ON s.VisitKey = c.VisitKey
    INNER JOIN MedRequestHeader h ON h.ClinicVisitKey = c.ClinicVisitKey
    INNER JOIN MedRequest r ON r.MedRequestHeaderKey = h.MedRequestHeaderKey
    WHERE s.VN = '${vn}'
    ORDER BY r.MedRequestKey`;
    return db.raw(sql);
  }

  drugByCheck(db: knex, cid: any, field: any, did: any, days: any) {
    const sql = `
    SELECT TOP 1 'op' as service,
    itm.Code AS drugcode
    ,mrq.ItemName AS drugname
    ,mqh.IssueDT AS vstdate
    ,mrq.IssueQty AS qty
    ,itm.BaseUnit AS units
    ,dit.Strength AS strength
    ,DATEDIFF(day,mqh.IssueDT,GETDATE()) AS dt
     
    FROM Visit AS vis
    LEFT JOIN MedRequestHeader AS mqh ON  mqh.VisitKey = vis.VisitKey
    LEFT JOIN MedRequest AS mrq ON mrq.MedRequestHeaderKey = mqh.MedRequestHeaderKey
    LEFT JOIN DrugItem AS dit ON dit.ItemKey = mrq.ItemKey
    LEFT JOIN Item AS itm ON itm.ItemKey = mrq.ItemKey
    LEFT JOIN Person AS pes ON pes.PersonKey = vis.PatientKey
    
    WHERE dit.ItemKey IN (${did})
    AND pes.Cid = '${cid}'
    AND mqh.IssueDT IS NOT NULL
    AND mrq.IssueQty > 0
    AND DATEDIFF(day,mqh.IssueDT,GETDATE()) <= ${days}`;
    return db.raw(sql);
  }

  //Labs

  labLists(db: knex, cid: any) {
    const sql = `SELECT
    lh.LabRequestHeaderKey as labordernumber,
    FORMAT(lh.DocDT, 'yyyy-MM-dd', 'en-us' ) AS orderdate,
    FORMAT (lh.DocDT, 'yyyy-MM-dd' , 'en-us') AS reportdate,
    FORMAT (lh.DocDT, 'yyyy-MM-dd' , 'en-us') AS receivedate,
    null as ordertime,null as reporttime,
    lh.Code as lab_code,
    STUFF( (
    SELECT ',' + lrq.ItemName
    FROM LabRequestHeader AS ls
    LEFT JOIN LabRequest AS lrq ON lrq.LabRequestHeaderKey = ls.LabRequestHeaderKey
    WHERE ls.LabRequestHeaderKey = lh.LabRequestHeaderKey
    ORDER BY lrq.ItemName ASC
    FOR XML PATH('')), 1, 1, '') formname,
    CASE
        WHEN lh.AdmitKey IS NULL THEN
        'OPD' 
        WHEN lh.AdmitKey IS NOT NULL THEN
        'IPD' 
    END AS department
    FROM Person AS ps
    LEFT JOIN LabRequestHeader AS lh ON lh.PatientKey = ps.PersonKey
    WHERE ps.Cid = '${cid}'
    AND (lh.CancelDT is null OR lh.CancelDT = '')
    ORDER BY lh.DocDT DESC,
    lh.LabTypeKey DESC`;
    return db.raw(sql);
  }

  labToDayResultAll(db: knex, vn: any) {
    const sql = `SELECT 
    r.ItemKey AS labcode,
    r.ItemName AS labname,
    u.ResultValue AS result,
    r.ItemUnitName AS unit,
    concat(u.MaxRefValue,' - ',u.MinRefValue) AS ref,
    n.Name AS remark,
    r.ApproverKey AS staff
    FROM LabRequestHeader h 
    INNER JOIN LabRequest r ON r.LabRequestHeaderKey = h.LabRequestHeaderKey
    INNER JOIN ClinicVisit c ON c.ClinicVisitKey = h.ClinicVisitKey
    inner JOIN Visit v ON v.VisitKey = c.VisitKey
    INNER JOIN LabItem i ON i.ItemKey = r.ItemKey
    INNER JOIN LabResult u ON u.LabRequestKey = r.LabRequestKey
    INNER JOIN Specimen n ON n.SpecimenKey = r.SpecimenKey
    WHERE v.VN = '${vn}'
    AND h.CancelDT IS NULL 
    AND r.ResultRecorderKey <> ''
    AND r.ResultRecorderKey IS NOT NULL
    AND ( r.ItemName NOT LIKE '%hiv%'
    OR r.ItemName NOT LIKE '%CD4%'
    OR r.ItemName NOT LIKE '%RPR%'
    OR r.ItemName NOT LIKE '%METHAMPHETAMINE%'
    OR r.ItemName NOT LIKE '%TPHA%'
    ) ORDER BY n.Name,r.ItemKey`;
    return db.raw(sql);
  }

  labByOrdernumber(db: knex, orn: any) {
    const sql = `SELECT
    lrqh.LabRequestHeaderKey as labcode,   
    lpo.NAME as labname,
    lrs.ResultValue as result,
    lpo.LabOutputUnit as unit,
    CASE
    WHEN (lrs.MinRefValue is not null or lrs.MaxRefValue is not null) THEN
    CONCAT(lrs.MinRefValue,' - ',lrs.MaxRefValue)
    ELSE lpo.NormalValueNote
    END AS ref,
    s.Name as remark
    FROM
    LabRequestHeader AS lrqh JOIN LabRequest AS lrq ON lrq.LabRequestHeaderKey = lrqh.LabRequestHeaderKey
    JOIN LabResult AS lrs ON lrs.LabRequestKey = lrq.LabRequestKey
    JOIN LabOutput AS lpo ON lpo.LabOutputKey = lrs.LabOutputKey
    JOIN LabLisTest AS llt ON llt.LabLisTestKey = lpo.LabLisTestKey
    JOIN LabRequest lr ON lrqh.LabRequestHeaderKey = lr.LabRequestHeaderKey 
    LEFT JOIN Specimen s ON lr.SpecimenKey = s.SpecimenKey 
    WHERE
    lrqh.LabRequestHeaderKey = '${orn}'
    AND lrq.ApproverKey IS NOT NULL
    AND lrs.ResultValue IS NOT NULL
    AND lrs.ResultValue != ''
    AND lpo.IsDisabled = 0
    AND lrs.ResultValue != ''
    AND ( lr.ItemName NOT LIKE '%hiv%'
    OR lr.ItemName NOT LIKE '%CD4%'
    OR lr.ItemName NOT LIKE '%RPR%'
    OR lr.ItemName NOT LIKE '%METHAMPHETAMINE%'
    OR lr.ItemName NOT LIKE '%TPHA%'
    )
    AND lpo.LabLisTestKey IN (
    SELECT
    llt.LabLisTestKey
    FROM
    LabLisTest AS llt
    LEFT JOIN LabOutput AS lop ON lop.LabLisTestKey = llt.LabLisTestKey
    )
    ORDER BY
    lrqh.LabRequestHeaderKey,
    lpo.LineNum`;
    return db.raw(sql);
  }

  //X-ray

  xrayLists(db: knex, cid: any) {
    return [];
  }

  xrayResult(db: knex, idx: any) {
    return [];
  }

  //pdf list

  pdfByVn(db: knex, vn: any) {
    return [];
  }
}
