import * as knex from 'knex';
import * as util from 'util';

export class HomcModel {

  async getHospital(db: knex) {
    const his = process.env.HOSP_CODE;
    const sql = await db.raw(`SELECT OFF_ID as hospcode,rtrim(NAME) as hospname FROM HOSPCODE where OFF_ID = '${his}'`);
    return sql;
  }

  countService(db: knex, cid: any) {
    const sql = `SELECT COUNT(cid) as count_vn FROM dbo.hie1_service('${cid}')`;
    return db.raw(sql);
  }

  visitAll(db: knex, cid: any) {
    const sql = `SELECT * FROM dbo.hie1_service('${cid}')`;
    return db.raw(sql);
  }

  visitScreening(db: knex, vn: any) {
    const sql = `SELECT * FROM dbo.hie1_screen('${vn}')`;
    return db.raw(sql);
  }

  visitDiag(db: knex, vn: any) {
    const sql = `SELECT * FROM dbo.hie1_diagnosis('${vn}')`;
    return db.raw(sql);
  }

  visitProcesure(db: knex, vn: any) {
    const sql = `SELECT * FROM dbo.hie1_procedure('${vn}')`;
    return db.raw(sql);
  }

  visitAppointment(db: knex, vn: any) {
    const sql = `SELECT * FROM dbo.hie1_appointment('${vn}')`;
    return db.raw(sql);
  }

  biopsyLists(db: knex, cid: any) {
    return [];
  }

  biopsyResult(db: knex, idx: any) {
    return [];
  }

  // Drugs

  drugAllergy(db: knex, cid: any) {
    const sql = `SELECT * FROM dbo.hie1_drugallergy('${cid}')`;
    return db.raw(sql);
  }

  drugByVn(db: knex, vn: any) {
    const sql = `SELECT * FROM dbo.hie1_drug('${vn}')`;
    return db.raw(sql);
  }

  drugByCheck(db: knex, cid: any, field: any, did: any, days: any) {
    return [{service: '', drugname: '', qty: '', unit: '', strength: '', vstdate: '', dt: 40}];
  }

  //Labs

  labLists(db: knex, cid: any) {
    const sql = `SELECT * FROM dbo.hie1_lab('${cid}') ORDER BY labordernumber DESC`;
    return db.raw(sql);
  }

  labToDayResultAll(db: knex, vn: any) {
    const sql = `SELECT * FROM dbo.hie1_lab_vn('${vn}') ORDER BY labordernumber DESC`;
    return db.raw(sql);
  }

  labByOrdernumber(db: knex, orn: any) {
    const sql = `SELECT * FROM dbo.hie1_lab_orn('${orn}') ORDER BY labordernumber DESC`;
    return db.raw(sql);
  }

  //X-ray

  xrayLists(db: knex, cid: any) {
    return [{vstdate: '', icode: '', name: '', xstatus: ''}];
  }

  xrayResult(db: knex, idx: any) {
    return [{result: ''}];
  }

  //pdf list

  pdfByVn(db: knex, vn: any) {
    return [];
  }
}
