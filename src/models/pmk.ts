import * as knex from 'knex';
import * as util from 'util';

export class PmkModel {

  async getHospital(db: knex) {
    const his = process.env.HOSP_CODE;
    const sql = await db.raw(`SELECT off_id as "hospcode",hospital_name as "hospname" FROM hospital_code WHERE off_id = '${his}'`);
    return sql;
  }

  countService(db: knex, cid: any) {
    const sql = `SELECT COUNT(o.opd_no) as "count_vn" FROM opds o
    INNER JOIN patients pt ON o.pat_run_hn = pt.run_hn AND o.pat_year_hn = pt.year_hn 
    WHERE pt.id_card = '${cid}'`;
    return db.raw(sql);
  }

  visitAll(db: knex, cid: any) {
    const sql = `SELECT o.opd_no AS "vn",o.pat_run_hn ||'/'|| o.pat_year_hn AS "hn",
    to_Char(o.opd_date,'yyyy-mm-dd') "vstdate",to_Char(o.opd_time,'hh:mm:ss') "vsttime",NULL AS "an",
    p.halfplace AS "spclty"
    FROM opds o
    INNER JOIN patients pt ON o.pat_run_hn = pt.run_hn AND o.pat_year_hn = pt.year_hn
    LEFT JOIN places p ON o.pla_placecode = p.placecode
    WHERE pt.id_card = '${cid}'
    AND o.opd_no NOT IN (
    SELECT DISTINCT  o.opd_no
    FROM opds o
    INNER JOIN patients pt ON o.pat_run_hn = pt.run_hn AND o.pat_year_hn = pt.year_hn
    LEFT  JOIN opddiags od ON od.opd_opd_no = o.opd_no
    INNER JOIN  icd10s id ON id.code = od.icd_code
    WHERE pt.id_card = '${cid}' AND
    id.code  IN ( 'Z21', 'R75', 'Y05' ) OR  id.code BETWEEN 'B200' AND 'B249' ) ORDER BY o.opd_no  DESC`;
    return db.raw(sql);
  }

  visitScreening(db: knex, vn: any) {
    const sql = `SELECT o.opd_no AS "vn",to_number(o.bp_systolic) AS "bps",to_number(o.bp_diastolic) AS "bpd",
    to_number(o.wt_kg) AS "bw",to_number(o.height_cm) AS "height",o.bmi as "bmi",to_number(o.respiratory_rate) AS "rr",
    to_number(o.temp_c) AS "temperature",to_number(o.waistline) AS "waist",null AS "hr",o.final_diagnosis AS "pe",null AS "fh",
    (select icd_code from disease_warehouse where opd_opd_no=o.opd_no and type=1) AS "hpi",
    null AS "pmh",null AS "sh",o.palse AS "pulse",o.symptom AS "cc",
    case
    when o.SMOKE_FLAG = 1 then 'ไม่สูบ'
    when o.SMOKE_FLAG = 2 then 'สูบนานๆครั้ง'
    when o.SMOKE_FLAG = 3 then 'สูบเป็นครั้งเป็นคราว'
    when o.SMOKE_FLAG = 4 then 'สูบเป็นประจำ'
    else 'ไม่ทราบ'
    end "smoking",
    case
    when o.ALCOHOL_FLAG = 1 then 'ไม่ดื่ม'
    when o.ALCOHOL_FLAG = 2 then 'ดื่มนานๆครั้ง'
    when o.ALCOHOL_FLAG = 3 then 'ดื่มเป็นครั้งเป็นคราว'
    when o.ALCOHOL_FLAG = 4 then 'ดื่มเป็นประจำ'
    else 'ไม่ทราบ'
    end "drinking",o.symptom as "symptom"
    FROM opds o
    WHERE o.opd_no = '${vn}'`;
    return db.raw(sql);
  }

  visitDiag(db: knex, vn: any) {
    const sql = `SELECT od.icd_code AS "icd10",od.type AS "diagtype",
    dt.name AS "diagtypename",id.icd_desc AS "icd10name"
    FROM opds o
    LEFT OUTER JOIN opddiags od ON opd_opd_no = o.opd_no
    INNER JOIN  icd10s id ON code = od.icd_code
    INNER JOIN  diagnosis_type dt ON diagnosis_type  = od.type
    WHERE o.opd_no = '${vn}' ORDER BY od.type`;
    return db.raw(sql);
  }

  visitProcesure(db: knex, vn: any) {
    const sql = `SELECT d.icd9cm_code as "icd9",s.name AS "opername"
    FROM opds o
    INNER JOIN DATA_EXCEPT_DRUG_WH d ON d.hn = o.pat_run_hn ||'/'|| o.pat_year_hn
    INNER JOIN service_codes s ON d.code = s.code
    WHERE o.opd_no = '${vn}'
    AND to_Char(o.opd_date,'dd/mm/yyyy') = to_Char(d.main_date,'dd/mm/yyyy')`;
    return db.raw(sql);
  }

  visitAppointment(db: knex, vn: any) {
    const sql = `SELECT to_Char(app_date,'yyyy-mm-dd') "nextdate",appoint_name AS "nexttime",
    p.halfplace AS "clinic",dd.appoint_place AS "contact",
    dd.note as "note",dd.doing_text AS "note1",null "note2"
    FROM date_dbfs dd
    INNER JOIN OPDS  o ON dd.pat_run_hn||'/'|| dd.pat_year_hn =  o.pat_run_hn||'/'|| o.pat_year_hn
    LEFT JOIN places p ON dd.pla_placecode = p.placecode
    WHERE o.opd_no = '${vn}'  `;
    return db.raw(sql);
  }

  biopsyLists(db: knex, cid: any) {
    return [{indx: '', itm: '', vstdate: ''}];
  }

  biopsyResult(db: knex, idx: any) {
    return [{result: ''}];
  }

  // Drugs

  drugAllergy(db: knex, cid: any) {
    const sql = `SELECT dc.genericname AS "agent",
    dc.drug_std_code AS "agentcode24",a.symptom as "symptom",a.allergic_desc AS "note",
    to_Char(a.date_created,'yyyy-mm-dd')  "reportdate",
    to_Char(a.doc_created,'yyyy-mm-dd') "begindate"
    FROM drug_pt_allergy a
    INNER JOIN patients pt ON a.pat_run_hn = pt.run_hn AND a.pat_year_hn = pt.year_hn
    INNER JOIN  drugcodes dc ON  a.dru_code = dc.code
    WHERE pt.id_card = 'cid'`;
    return db.raw(sql);
  }

  drugByVn(db: knex, vn: any) {
    const sql = `SELECT d.name AS "name",df.quantity AS "qty",
    u.name || t.name || w.name AS "shortist"
    FROM DRUG_FINANCE_DETAILS df
    INNER JOIN  OFH_WH ow ON ow.opd_finance_no = df.ofh_opd_finance_no
    INNER JOIN OPDS o ON  ow.hn = o.pat_run_hn ||'/'|| o.pat_year_hn
    INNER JOIN DRUGCODES d ON df.dru_code = d.code
    LEFT JOIN DRUG_USING_CODES u ON  d.duc_using_code = u.using_code
    LEFT JOIN DRUG_TIMING_CODES t ON  d.dtc_timing_code = t.timing_code
    LEFT JOIN DRUG_WARNING_CODES w ON d.dwc_warning_code = w.warning_code
    WHERE o.opd_no = '${vn}'
    AND to_Char(o.opd_date,'dd/mm/yyyy') = to_Char(df.date_created,'dd/mm/yyyy')
    ORDER BY   df.ofh_opd_finance_no DESC`;
    return db.raw(sql);
  }

  drugByCheck(db: knex, cid: any, field: any, did: any, days: any) {
    const sql = `SELECT d.code AS "drugcode",d.name AS "drugname",df.quantity AS "qty",n.name AS "units",
    d.strength,( CURRENT_DATE - o.opd_date ) AS "dt"
    FROM DRUG_FINANCE_DETAILS df
    INNER JOIN  OFH_WH ow ON ow.opd_finance_no = df.ofh_opd_finance_no
    INNER JOIN PATIENTS pt ON ow.hn = pt.hn
    INNER JOIN OPDS o ON  ow.hn = o.pat_run_hn ||'/'|| o.pat_year_hn
    INNER JOIN DRUGCODES d ON df.dru_code = d.code
    LEFT JOIN DRUG_UNIT_CODES n ON d.duc1_unit_code = n.unit_code
    WHERE d.drug_std_code LIKE '${did}'
    AND pt.id_card = '${cid}'
    AND df.quantity > '0' AND ( sysdate - o.opd_date ) <= '${days}'`;
    return db.raw(sql);
  }

  //Labs

  labLists(db: knex, cid: any) {
    const sql = `SELECT ls.labresult_no "labordernumber",ld.name "formname",
    To_char(ls.date_created,'yyyy-mm-dd') "orderdate",
    To_char(ls.date_created,'hh24:mi:ss')  "ordertime",
    To_char(ls.report_datetime,'yyyy-mm-dd')  "reportdate",
    To_char(ls.report_datetime,'hh24:mi:ss')  "reporttime",
    NVL(To_char(ls.report_datetime,'yyyy-mm-dd'),To_char(ls.date_created,'yyyy-mm-dd'))  "receivedate",
    NVL(To_char(ls.report_datetime,'hh24:mi:ss'),To_char(ls.date_created,'hh24:mi:ss'))  "receivetime",
    p.halfplace "department"
    FROM  labresult ls
    INNER JOIN ofh_wh w ON w.opd_finance_no = ls.ofh_opd_finance_no
    INNER JOIN PATIENTS pt ON w.hn = pt.hn
    INNER JOIN OPDS o ON  w.hn = o.pat_run_hn ||'/'|| o.pat_year_hn
    LEFT JOIN  labcodes l ON ls.lab1_labcode = l.labcode
    LEFT JOIN labcode_detail ld ON ls.labcode_detail_code = ld.labcode_detail_code
    LEFT  JOIN places p ON p.placecode = o.pla_placecode
    WHERE pt.id_card = '${cid}'
    AND ls.final_character_result_state = 'Y'
    AND to_Char(o.opd_date,'dd/mm/yyyy') = to_Char(ls.date_created,'dd/mm/yyyy')
    AND l.labcode NOT IN ( SELECT labcode FROM labcodes WHERE UPPER (labname) LIKE'%HIV%' )
    AND o.opd_no NOT IN ( SELECT   o.opd_no FROM opds o
    INNER JOIN patients pt ON o.pat_run_hn||'/'||o.pat_year_hn = pt.hn
    LEFT  JOIN opddiags od ON opd_opd_no = o.opd_no
    LEFT JOIN  icd10s id ON code = od.icd_code
    WHERE pt.id_card = '${cid}'
    AND id.code BETWEEN 'B200' AND 'B249' ) ORDER BY   ls.date_created desc`;
    return db.raw(sql);
  }

  labToDayResultAll(db: knex, vn: any) {
    const sql = `SELECT o.opd_no AS "vn",
    ls.labcode_detail_code AS "labcode",
    ld.name AS "labname",ls.min_normal_value ||' - '|| ls.max_normal_value "ref",
    ls.numberic_result AS result,ld.unit_text AS unit,ls.remark,ls.user_created AS "staff"
    FROM  labresult ls
    INNER JOIN ofh_wh w ON w.opd_finance_no = ls.ofh_opd_finance_no
    INNER JOIN OPDS o ON  w.hn = o.pat_run_hn ||'/'|| o.pat_year_hn
    LEFT JOIN  labcodes l ON ls.lab1_labcode = l.labcode
    LEFT JOIN labcode_detail ld ON ls.labcode_detail_code = ld.labcode_detail_code
    WHERE o.opd_no = '${vn}'
    AND to_Char(o.opd_date,'dd/mm/yyyy') = to_Char(ls.date_created,'dd/mm/yyyy')
    AND l.labcode NOT IN ( SELECT labcode FROM labcodes WHERE UPPER (labname) LIKE'%HIV%' )
    AND l.labcode NOT IN ( SELECT labcode FROM labcodes WHERE UPPER (labname) LIKE'%CD4%' )
    AND l.labcode NOT IN ( SELECT labcode FROM labcodes WHERE UPPER (labname) LIKE'%VDRL/RPR)%' )
    AND l.labcode NOT IN ( SELECT labcode FROM labcodes WHERE UPPER (labname) LIKE'%METHAMPHETAMINE%' )
    AND l.labcode NOT IN ( SELECT labcode FROM labcodes WHERE UPPER (labname) LIKE'%TPHA%' )
    AND l.labcode NOT IN ( SELECT labcode FROM labcodes WHERE UPPER (labname) LIKE'%PHOSPHATASE%' )`;
    return db.raw(sql);
  }

  labByOrdernumber(db: knex, orn: any) {
    const sql = `SELECT o.opd_no AS "vn",
    ls.labcode_detail_code AS "labcode",
    ld.name AS "labname",ls.min_normal_value ||' - '|| ls.max_normal_value "ref",
    ls.numberic_result AS result,ld.unit_text AS unit,ls.remark,ls.user_created AS "staff"
    FROM  labresult ls
    INNER JOIN ofh_wh w ON w.opd_finance_no = ls.ofh_opd_finance_no
    INNER JOIN OPDS o ON  w.hn = o.pat_run_hn ||'/'|| o.pat_year_hn
    LEFT JOIN  labcodes l ON ls.lab1_labcode = l.labcode
    LEFT JOIN labcode_detail ld ON ls.labcode_detail_code = ld.labcode_detail_code
    WHERE ls.labresult_no = '${orn}'
    AND to_Char(o.opd_date,'dd/mm/yyyy') = to_Char(ls.date_created,'dd/mm/yyyy')
    AND l.labcode NOT IN ( SELECT labcode FROM labcodes WHERE UPPER (labname) LIKE'%HIV%' )
    AND l.labcode NOT IN ( SELECT labcode FROM labcodes WHERE UPPER (labname) LIKE'%CD4%' )
    AND l.labcode NOT IN ( SELECT labcode FROM labcodes WHERE UPPER (labname) LIKE'%VDRL/RPR)%' )
    AND l.labcode NOT IN ( SELECT labcode FROM labcodes WHERE UPPER (labname) LIKE'%METHAMPHETAMINE%' )
    AND l.labcode NOT IN ( SELECT labcode FROM labcodes WHERE UPPER (labname) LIKE'%TPHA%' )
    AND l.labcode NOT IN ( SELECT labcode FROM labcodes WHERE UPPER (labname) LIKE'%PHOSPHATASE%' )`;
    return db.raw(sql);
  }

  //X-ray

  xrayLists(db: knex, cid: any) {
    return [{vstdate: '', icode: '', name: '', xstatus: ''}];
  }

  xrayResult(db: knex, idx: any) {
    return [{result: ''}];
  }

  //pdf list

  pdfByVn(db: knex, vn: any) {
    return [];
  }
}
